package com.example.tiposdedatos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tiposDeDatos()
    }

    private fun tiposDeDatos(){

        // String

        val myString : String = "¡Hello, user!"
        val myString2 = "Welcome to TinPet."
        val myString3 = myString + " " + myString2

        println(myString3)

        // Enteros (Byte, Short, Int, Long)

        val myInt : Int = 1
        val myInt2 = 2
        val myInt3 = myInt + myInt2
        println(myInt3)

        // Decimales (Float, Double)

        val myFloat : Float = 1.5f
        val myDouble = 1.5
        val myDouble2 = 2.6
        val myDouble3 = 1 // ESTO ES UN INT
        val myDouble4 = myDouble + myDouble2 + myDouble3
        println(myDouble4)

        // Boolean (Bool)

        val myBool : Boolean = true
        val myBool2 = false

        // val myBool3 = myBool + myBool2

        println(myBool == myBool2)
    }
}