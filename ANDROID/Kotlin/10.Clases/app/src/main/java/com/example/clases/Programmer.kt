package com.example.clases

class Programmer(val name: String,
                 var age: Int,
                 val languages: Array<Lenguage>,
                 val friends: Array<Programmer>? = null) {

    enum class Lenguage{
        JAVA,
        C,
        KOTLIN,
        HTML
    }
    fun code(){
        for (lenguage in languages){
            println("Estoy programando en $lenguage")
        }
    }
}