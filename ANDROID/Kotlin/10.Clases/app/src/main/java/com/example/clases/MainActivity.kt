package com.example.clases

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        classes()
    }

    fun classes(){

        val p1 = Programmer("Roy", 23, arrayOf(Programmer.Lenguage.JAVA, Programmer.Lenguage.C))
        println(p1.name)

        p1.age = 30
        p1.code()

        val p2 = Programmer("Joel", 23, arrayOf(Programmer.Lenguage.KOTLIN, Programmer.Lenguage.HTML), arrayOf(p1))
        p2.code()
        println("${p2.friends?.first()?.name} es amigo de ${p2.name}")

    }
}