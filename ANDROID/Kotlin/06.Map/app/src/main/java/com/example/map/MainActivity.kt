package com.example.map

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        maps()
    }

    fun maps(){

        //Sintaxis
        var myMap:Map<String, Int> = mapOf()
        println(myMap)

        // Añadir elementos
        myMap = mutableMapOf("Roy" to 1, "Juan" to 2, "Luis" to 5)
        println(myMap)

        // Añadir un solo valor
        myMap["Sara"] = 7
        myMap.put("María",8)
        println(myMap)

        // Actualizar un dato
        myMap.put("Roy", 3)
        myMap["Roy"]=4
        println(myMap)

        myMap.put("Marcos", 3)
        println(myMap)

        // Acceso a un dato
        println(myMap["Roy"])

        // Eliminar un dato
        myMap.remove("Roy")
        println(myMap)


    }
}