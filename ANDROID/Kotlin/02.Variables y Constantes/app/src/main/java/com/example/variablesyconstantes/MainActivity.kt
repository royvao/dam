package com.example.variablesyconstantes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        variablesYConstantes()
    }


    private fun variablesYConstantes(){

        // Variables

        var myFirstVariable = "¡Welcome to TinPet!"
        var myFirstNumber = 1

        println(myFirstVariable)

        myFirstVariable = "¡Bienvenidos a TinPet!."

        println(myFirstVariable)

        //myFirstVariable = 1

        var mySecondVariable = "¡Regístrate!"
        println(mySecondVariable)
        mySecondVariable = myFirstVariable
        println(mySecondVariable)

        myFirstVariable = "¿Ya te has registrado?"
        println(myFirstVariable)

        // Constantes

        val myFirstConstant = "¿Te está gustando la aplicación?"

        println(myFirstConstant)

        // myFirstConstant = "¡Valora con una reseña!"

        val mySecondConstant = myFirstVariable
        println(mySecondConstant)
    }
}