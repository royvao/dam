package com.example.sentenciawhen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sentenciaWhen()
    }

    fun sentenciaWhen(){

        // When con String
        val country = "Argentina"

        when (country){
            "España", "México", "Argentina", "Perú", "Colombia" -> {
                println("El idioma es Español")
            }
            "Francia" -> {
                println("El idioma es Francés (FR)")
            }
            "EEUU" -> {
                println("El idioma es Inglés (USA)")
            }
            "Inglaterra" -> {
                println("El idioma es Inglés (ENG)")
            } else -> {
                println("Idioma no identificado.")
            }

        }

        // When con Int
        val age = -10

        when(age){
            0, 1, 2 -> {
                println("Eres un bebé")
            }
            in 3..10 -> {
                println("Eres un niño")
            }
            in 11..17 -> {
                println("Eres un adolescente")
            }
            in 18..69 -> {
                println("Eres un adulto")
            }
            in 70..99 -> {
                println("Eres un anciano")
            }else -> {
                println("Edad no comprendida entre 0 y 99")
            }
        }
    }
}