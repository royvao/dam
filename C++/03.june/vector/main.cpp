#include <iostream>

#include "vector.h"

using namespace std;

void imprimir (Vector v){
	cout << v.x << v.y << v.z;
}

int main(){
	Vector v0= Vector (2, 3, 5); // Asignamos el valor con un constructor
	Vector v1(5, 2, 3); // Asignamos el valor directamente
	Vector result = v0 + v1; // Objeto result es la suma de los dos vectores

	imprimir (v0); // Imprime el primer vector
	cout << " + ";
	imprimir (v1); // Imprime segundo vector
	cout << " = ";
	imprimir (result); // Imprime resultado
	cout << "\n";

	return 0;
}
