#include <iostream>
#include "encapsulamiento.h"

encapsulamiento::encapsulamiento(std::string _nombre, unsigned int _edad){
	this->nombre=_nombre;
	this->edad=_edad;
}

std::string encapsulamiento::getNombre(){
	return this->nombre;
}

void encapsulamiento::setNombre (std::string value){
	this->nombre=value;
}

unsigned int encapsulamiento::getEdad(){
	return this->edad;
}

void encapsulamiento::setEdad (unsigned int value){
	this->edad=value;
}




