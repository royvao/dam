#include <iostream>
#include "string.h"

using namespace std;

String::String(string _nombre, string _contraseña){
	this->nombre=_nombre;
	this->contraseña=_contraseña;
}

string String::getNombre(){
	return nombre;
}

void String::setNombre(string value){
	this->nombre=value;
}

string String::getContraseña(){
	return contraseña;
}

void String::setContraseña(string value){
	this->contraseña=value;
}

