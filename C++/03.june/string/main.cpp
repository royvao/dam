#include <iostream>
#include "string.h"

void askName(string *nombre /*Con puntero*/){
	cout << "Nombre: ";
	cin >> *nombre;
}

void askPasswd(string contraseña /*Sin puntero*/){
	cout << "Contraseña: ";
	cin >> contraseña;
}

void menu (){
	system ("clear");
	cout << "\nINICIO DE SESIÓN\n";
}

int main(){
	string nombre;
	string contraseña;
	
	menu();
	askName(&nombre); /* Para llamar al puntero */
	askPasswd(contraseña);

	String user1(nombre, contraseña);
	
	if(user1.getNombre() == "roy" && user1.getContraseña() == "1234"){
		system ("clear");
		cout << "\n\n CONTECTANDO...\n";
		cout << "Bienvenido, " << user1.getNombre() << ".\n";
	}
/*	
	else if(user1.getNombre() == "roy" && user1.getContraseña() != "1234"){
		cout << "Contraseña incorrecta.\n";
	}*/

	return 0;
}
