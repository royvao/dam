#ifndef CARNIVORO_H
#define CARNIVORO_H

#include <iostream>
#include "animal.h"

using namespace std;

class Carnivoro: /*protected Animal*/ public Animal{
	public:
		unsigned int getN_patas();
		unsigned int getN_ojos();
		string getNombre();

		Carnivoro(unsigned int patas, unsigned int ojos, string _nombre);

		virtual	void dieta(); // IMPORTANTE RECALCAR QUE ES VIRTUAL 
};
#endif // CARNIVORO_H
