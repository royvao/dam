#include <iostream>
#include "carnivoro.h"

using namespace std;

Carnivoro::Carnivoro(unsigned int patas, unsigned int ojos, string _nombre){
	this->npatas=patas;
	this->nojos=ojos;
	this->nombre=_nombre;
}

void Carnivoro::dieta(){
	cout << "Tengo " << this->npatas << " patas, " << this->nojos << " ojos y me llamo " << this->nombre << "\n";
}

unsigned int Carnivoro::getN_patas(){
	return npatas;
}

unsigned int Carnivoro::getN_ojos(){
	return nojos;
}

string Carnivoro::getNombre(){
	return nombre;
}

