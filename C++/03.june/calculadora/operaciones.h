#ifndef OPERACIONES_H
#define OPERACIONES_H

#include "menu.h"

class Operaciones{
	private:
		int op1;
		int op2;
		int result;
		void insertar();
		void imprimir();
	public:
		Operaciones();
		void run();
		void suma();
		void resta();
		void multiplicacion();
		void division();
};

#endif // OPERACIONES_H
