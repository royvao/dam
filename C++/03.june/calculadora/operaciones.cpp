#include <stdlib.h>
#include <iostream>

#include "operaciones.h"

using namespace std;

Operaciones::Operaciones(){
	op1=0;
	op2=0;
	result=0;
}

/* OPERACIONES */

void Operaciones::suma(){
	this->insertar();
	result = op1 + op2;
	this->imprimir();
}

void Operaciones::resta(){
	this->insertar();
	result = op1 - op2;
	this->imprimir();
}

void Operaciones::multiplicacion(){
	this->insertar();
	result = op1 * op2;
	this->imprimir();
}

void Operaciones::division(){
	this->insertar();
	result = op1 / op2;
	this->imprimir();
}

/* PEDIR DATOS AL USUARIO */

void Operaciones::insertar(){
	cout << "Introduzca el primer número: ";
	cin >> op1;

	cout << "Introduzca el segundo número: ";
	cin >> op2;
}

/* IMPRIMIR RESULTADO */

void Operaciones::imprimir(){
	cout << "El resultado es: " << result << "\n";
}
