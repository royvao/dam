#include "menu.h"
#include "operaciones.h"

int main(){
	/* VARIABLES y OBJETOS*/
	int user;
	Operaciones Op;
	Opt opt;

	/* METODOS */
	opt.run();
	user = opt.ask_menu();

	/* ALGORITMIA MENU */

	if (user == 1){
		Op.suma();
	}else if (user == 2){
		Op.resta();
	}else if (user == 3){
		Op.multiplicacion();
	}else if (user == 4){
		Op.division();
	}

	return 0;
}

