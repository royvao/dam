#include <stdlib.h>
#include <iostream>

#include "menu.h"

using namespace std;
const char *optmenu[]{
	"Suma",
	"Resta",
	"Multiplicación",
	"División"
};

void Opt::run(){
	this->title();
	this->menu();
}

OpMenu Opt::menu(){
	for (int i=0; i < total-1; i++)
		cout << i+1 << "-" << optmenu[i] << "\n";
	return (OpMenu) 1;
}

OpMenu Opt::ask_menu(){
	int n;
	cout << "\nSeleccione una operación (1-4): ";
	cin >> n;
	return (OpMenu) n;
}

void Opt::title(){
	system ("clear");
	cout << "CALCULADORA\n\n";
}
