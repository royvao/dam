#ifndef MENU_H
#define MENU_H

enum OpMenu {
	nll,
	sum,
	res,
	dvd,
	nul,
	total
};

class Opt{
	public:
		void run();
		void title();
		OpMenu menu();
		OpMenu ask_menu();
};

#endif // MENU_H
