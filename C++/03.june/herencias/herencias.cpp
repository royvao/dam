#include <iostream>
#include "herencias.h"

Coche::Coche(std::string _marca, unsigned int _nruedas){
	this->marca=_marca;
	this->nruedas=_nruedas;
}

std::string Coche::getMarca(){
	return this->marca;
}

void Coche::setMarca(std::string value){
	this->marca=value;
}

unsigned int Coche::getNruedas(){
	return this->nruedas;
}

void Coche::setNruedas(unsigned int value){
	this->nruedas=value;
}

