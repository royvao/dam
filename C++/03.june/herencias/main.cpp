#include <iostream>
#include "coche.h"
#include "van.h"

int main (){
	/* POLIMORFISMO */
	Coche *coche[2] ;
	coche[0]= new Deportivo("Ferrari",4,true);
	coche[1]= new Van("Volkswagen",5,60);

	for (int i=0; i<2; i++){
		coche[i]->acelerar();
		std::cout << " y tiene " << coche[i]->getNruedas() << " ruedas" << "\n";
	}

return 0;
}
