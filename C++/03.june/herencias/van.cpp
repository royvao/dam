#include <iostream>
#include "van.h"

Van::Van(std::string _marca, unsigned int _nruedas, unsigned int _capacidad):Coche(_marca,_nruedas){
	this->capacidad=_capacidad;
}

unsigned int Van::getCapacidad(){
	return this->capacidad;
}

void Van::setCapacidad(unsigned int value){
	this->capacidad=value;
}

void Van::acelerar(){
	std::cout << "La furgoneta " << Van::getMarca() << " está acelerando";
}
