#include <iostream>
#include "coche.h"

Deportivo::Deportivo(std::string _marca, unsigned int _nruedas, bool _aleron):Coche(_marca,_nruedas){
	this->aleron=_aleron;
}

bool Deportivo::getAleron(){
	return this->aleron;
}

void Deportivo::setAleron(bool value){
	this->aleron=value;
}

void Deportivo::acelerar(){
	std::cout << "El coche " << Deportivo::getMarca() << " está acelerando";
}
