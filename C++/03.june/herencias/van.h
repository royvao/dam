#ifndef VAN_H
#define VAN_H
#include <iostream>
#include "herencias.h"

class Van: public Coche{
	private:
		unsigned int capacidad;
	public:
		Van(std::string _marca, unsigned int _nruedas, unsigned int _capacidad);
		unsigned int getCapacidad();
		void setCapacidad(unsigned int);
		virtual void acelerar();
};
#endif // VAN_H

