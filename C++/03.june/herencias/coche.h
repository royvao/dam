#ifndef COCHE_H
#define COCHE_H
#include <iostream>
#include "herencias.h"

class Deportivo: public Coche{
	private:
		bool aleron;
	public:
		Deportivo(std::string _marca, unsigned int _nruedas, bool aleron);
		virtual void acelerar();
		bool getAleron();
		void setAleron(bool);
		
			
};
#endif // COCHE_H
