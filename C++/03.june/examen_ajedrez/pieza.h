#ifndef PIEZA_H
#define PIEZA_H

#include "desplazamiento.h"

class Pieza{
	private:
		inline static unsigned int total_piezas=0;
		unsigned int id=0;
	public:
		Pieza();
		virtual Desplazamiento get_desplazamiento(unsigned int)=0;
		virtual const Desplazamiento *list_desplazamiento()=0;
};

#endif // PIEZA_H
