#ifndef POSICION_H
#define POSICION_H

#include "desplazamiento.h"

class Posicion{
	public:
		int fila;
		int columna;

		Posicion& operator+(const Desplazamiento & desplazamiento);
};

#endif // POSICION_H
