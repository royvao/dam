#ifndef PEON_H
#define PEON_H

#include "pieza.h"

class Peon:public Pieza{
	private:
		inline static Desplazamiento desplazamientos[4]={
			{-1, 0},
			{ 1, 0},
			{-1,-1},
			{-1, 1}	
		};
	public:
		virtual Desplazamiento get_desplazamiento(unsigned int idx);
		virtual Desplazamiento *list_desplazamiento();
};

#endif // PEON_H
