#include <iostream>
#include <vector>

using namespace std;
void menu(){
	system ("clear");
	cout << "VECTOR\n\n";
}

int main(){
	vector<int> vct;
	int numeros;
	
	menu();
	cout << "Número a introducir (negativo detiene): \n";
	do{
		cin >> numeros;
		vct.push_back(numeros);
	}while(numeros>0);
	cout << "El total de números añadidos al vector es: " << int(vct.size()) << "\n";

	return 0;
}

