#ifndef STATIC_H
#define STATIC_H

class Suma{
	private:
		inline static int nobjetos = 0; // No necesito constructor porque uso inline
		int aumentar;
	public:
		Suma(); // Constructor
		int getAumentar(); // Método get
		static int getN_objetos(); // Método get static
		static double sumar(int op1, int op2); // Método static con dos parámetros
};

#endif // STATIC_H
