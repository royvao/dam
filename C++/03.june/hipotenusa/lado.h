#ifndef LADO_H
#define LADO_H

class Triangulo{
	private: 
		double cateto;
	public:
		Triangulo(double n=0);
		double operator+(Triangulo &c) const;

		double getCateto() const;
		void setCateto(double);
};
#endif // LADO_H
