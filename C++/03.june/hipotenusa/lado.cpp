#include <iostream>
#include <cmath>

#include "lado.h"

Triangulo::Triangulo(double n): cateto(n){}

double Triangulo::getCateto() const{
	return this->cateto;
}

void Triangulo::setCateto (double n){
	this->cateto=n;
}

/* MÉTODO CONST, NO SE MODIFICAN ATRIBUTOS */

double Triangulo::operator+(Triangulo &c) const{
	return sqrt(pow(this->cateto, 2)+pow(c.cateto, 2));
}
