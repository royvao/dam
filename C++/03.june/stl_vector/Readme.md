## Vector

```cpp
#include <iostream>	
#include <vector>

std::vector<>;
```

Características:
1. Se tiene que comportar como un vector.
1. Es un objeto estático.
1. Las celdas se resevan dinámicamente.
1. `push_back` es quien reserva la memoria.
1. La memoria se va "doblando" cuando se agota.
1. `size()` muestra la cantidad de elementos ocupados.
1. `capacity()` la cantidad de elementos reservados.
1. `max_size()` máximo número teórico de elementos.
1. `reserve(size_t)` realiza reserva inicial.
1. `emplace_back()`.
	
