#include <vector>
#include <iostream>

int main(){
	std::vector<int> vect; // DECLARAS EL VECTOR DE TIPO int SIN UN TAMAÑO
	int num;

	std::cout << "Cuántos números quieres en el vector: ";
	std::cin >> num;

	for (int i=0; i< num; i++){
		int numero;
		std::cout << i+1 << "º número: ";
		std::cin >> numero;

		vect.push_back(numero); // PODEMOS METER SIN PROBLEMA CADA NUMERO EN EL VECTOR PORQUE NO TIENE LÍMITE
	}

	std::cout << "\n";
	
	for (int i=0; i < vect.size(); i++){ // LE PASAMOS EL TAMAÑO DEL VECTOR CON size()
		std::cout << "La celda " << i+1 << " contiene: " << vect[i];
		std::cout << "\n";
	}
	return 0;
}
