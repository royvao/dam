#include <iostream>
#include <vector>

using namespace std;
#define TIPO int // DEFINIMOS EL TIPO DE DATO POR SI QUEREMOS CAMBIARLO 

void imprimir(const vector<TIPO>& num){
	for (const TIPO& valor : num)
		cout << valor << " ";
	cout << "\n\n";
}

int main(){
	
	vector<TIPO> numeros;
	
	/* INSERTAR ATRAS */
	cout << "\n***INSERTAR ATRÁS CON vector.push_back(x)***\n";
	numeros.push_back(10);
	numeros.push_back(20);
	numeros.push_back(30);
	numeros.push_back(40);
	imprimir(numeros);
	
	/*
	cout << numeros.at(0) << "\n"; // UTILIZAMOS EL MÉTODO at() PARA CONSULTAR QUÉ HAY EN LA CELDA  QUE LE PASEMOS
	cout <<	numeros.at(1) << "\n";
	cout <<	numeros.at(2) << "\n";
	cout <<	numeros.at(3) << "\n";
	*/

	/* INSERTAR DELANTE */
	cout << "\n***INSERTAR DELANTE CON vector.insert(puntero, x)***\n";
	numeros.insert(numeros.begin(), 100); // LE PASAMOS COMO REFERENCIA UN PUNTERO A LA PRIMERA CELDA SEGUIDO DEL VALOR QUE QUEREMOS AÑADIR
	imprimir(numeros);

	/* INSERTAR EN UNA CELDA ESPECÍFICA */
	cout << "\n***INSERTAR EN UNA CELDA ESPECÍFICA CON vector.insert(vector.begin()+y, x)***\n";
	numeros.insert(numeros.begin()+2 , 3);
	imprimir(numeros);

	/* MODIFICAR UNA CELDA */
	cout << "\n***MODIFICAR UNA CELDA CON vector[x]=x***\n";
	numeros[1]=1;
	imprimir(numeros);


	/* ELIMINAR ATRÁS */
	cout << "***\nELIMINAR ATRÁS CON vector.pop_back()***\n";
	numeros.pop_back();
	imprimir(numeros);
	
	/* ELIMINAR DELANTE */
	cout << "\n***ELIMINAR DELANTE CON vector.erase(vector.begin())***\n";
	numeros.erase(numeros.begin());
	imprimir(numeros);

	/* ELIMINAR EN UNA CELDA ESPECÍFICA */
	cout << "\n***ELIMINAR EN UNA CELDA ESPECÍFICA CON vector.erase(vector.begin()+y)***\n";
	numeros.erase(numeros.begin()+2);
	imprimir(numeros);

	/* ELIMINAR TODOS LOS ELEMENTOS DEL VECTOR */
	cout << "\n***ELIMINAR TODOS LOS ELEMENTOS CON vector.clear()***\n";
	numeros.clear();
	imprimir(numeros);

	return 0;
}
