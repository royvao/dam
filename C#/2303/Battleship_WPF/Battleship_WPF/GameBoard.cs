﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship_WPF
{
    public class GameBoard
    {
        public enum ResultadoDisparo
        {
            Agua,
            Tocado,
            Hundido
        }

        public Cell[,] Cells { get; set; }

        public GameBoard(int size)
        {
            Cells = new Cell[size, size];
            for (int row = 0; row < size; row++)
            {
                for (int col = 0; col < size; col++)
                {
                    Cells[row, col] = new Cell { Row = row, Column = col, IsEmpty = true };
                }
            }
        }

        public bool PlaceShip(Ship ship, int row, int col, Direccion direction)
        {
            // Verificamos si la posición inicial es válida
            if (row < 0 || row >= Cells.GetLength(0) || col < 0 || col >= Cells.GetLength(1))
            {
                return false;
            }

            // Verificamos si hay suficiente espacio para el barco
            int size = ship.Size;
            if ((direction == Direccion.Horizontal && col + size > Cells.GetLength(1))
                || (direction == Direccion.Vertical && row + size > Cells.GetLength(0)))
            {
                return false;
            }

            // Verificamos si las celdas donde se colocará el barco están vacías
            List<Cell> cells = new List<Cell>();
            for (int i = 0; i < size; i++)
            {
                int r = direction == Direccion.Horizontal ? row : row + i;
                int c = direction == Direccion.Horizontal ? col + i : col;
                if (!Cells[r, c].IsEmpty)
                {
                    return false;
                }
                cells.Add(Cells[r, c]);
            }

            // Si todas las celdas están vacías, colocamos el barco
            ship.Cells = cells;
            foreach (Cell cell in cells)
            {
                cell.IsEmpty = false;
                cell.Ship = ship;
            }
            return true;
        }

        public ResultadoDisparo FireShot(int row, int col)
        {
            var cell = Cells[row, col];
            if (cell.IsHit)
            {
                return ResultadoDisparo.Agua;
            }
            cell.IsHit = true;
            if (cell.HasShip)
            {
                return ResultadoDisparo.Tocado;
            }
            return ResultadoDisparo.Agua;
        }
    }

}
