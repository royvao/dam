﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship_WPF
{
    public enum Direccion
    {
        Horizontal,
        Vertical
    }


    public class Ship
    {
        public string Name { get; set; }
        public int Size { get; set; }
        public int Life { get; set; }
        public int HitCount { get; set; }
        public List<Cell> Cells { get; set; }

        public Ship(string name, int size)
        {
            Name = name;
            Size = size;
            Life = size;
            Cells = new List<Cell>(); // inicializamos la lista de celdas
        }
    }



}
