﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Battleship_WPF
{
    public partial class ChangePasswordPage : Page
    {
        #region components
        private readonly UserViewModel _userViewModel;
        string connectionString = @"Data Source=(local);Initial Catalog=Usuarios;Integrated Security=True";
        string _selectQuery = "SELECT * FROM usersData WHERE userName = @usuario";
        string _updateQuery = "UPDATE usersData SET userPassword = @contraseña WHERE userName = @usuario";
        #endregion
        public ChangePasswordPage(UserViewModel userViewModel)
        {
            #region info
            InitializeComponent();
            _userViewModel = userViewModel;
            Loaded += ChangePasswordPage_Loaded;
            #endregion
        }

        #region changePasswordPage_Loaded
        private void ChangePasswordPage_Loaded(object sender, RoutedEventArgs e)
        {
            Window parentWindow = Window.GetWindow(this);
            parentWindow.Width = 500;
            parentWindow.Height = 600;
        }
        #endregion

        #region chngpwdButton_Click
        private void ChngpwdButton_Click(object sender, RoutedEventArgs e)
        {
            string oldPassword = actualPwdBox.Password;
            string newPassword1 = newPasswordBox.Password;
            string newPassword2 = newPassword2Box.Password;

            if (string.IsNullOrWhiteSpace(oldPassword))
            {
                apLbl.Content = "Debe ingresar la contraseña actual.";
                npw2Lbl.Content = "";
                npwLbl.Content = "";
                return;
            }
            else if (string.IsNullOrWhiteSpace(newPassword1))
            {
                npwLbl.Content = "Debe ingresar una contraseña nueva.";
                npw2Lbl.Content = "";
                apLbl.Content = "";
                return;
            }
            else if (string.IsNullOrWhiteSpace(newPassword2))
            {
                npw2Lbl.Content = "Debe repetir la contraseña nueva.";
                npwLbl.Content = "";
                apLbl.Content = "";
                return;
            }
            else if (newPassword1 != newPassword2)
            {
                apLbl.Content = "";
                npwLbl.Content = "";
                npw2Lbl.Content = "";
                errorMessageTextBlock.Text = "Las contraseñas no coinciden.";
            }
            else if (oldPassword == newPassword1)
            {
                apLbl.Content = "";
                npwLbl.Content = "";
                npw2Lbl.Content = "";
                errorMessageTextBlock.Text = "Las nueva contraseña es igual que la actual.";
            }
            else
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    // Busca al usuario en la base de datos
                    SqlCommand selectCommand = new SqlCommand(_selectQuery, connection);
                    selectCommand.Parameters.AddWithValue("@usuario", _userViewModel.userName);
                    SqlDataReader reader = selectCommand.ExecuteReader();

                    if (reader.Read())
                    {
                        // Si se encuentra al usuario, comprueba la contraseña
                        string actualPassword = reader.GetString(reader.GetOrdinal("userPassword"));
                        reader.Close(); // <-- Cerrar la lectura de datos aquí
                        if (oldPassword == actualPassword)
                        {
                            // Si la contraseña es correcta, actualiza la contraseña en la base de datos
                            SqlCommand updateCommand = new SqlCommand(_updateQuery, connection);
                            updateCommand.Parameters.AddWithValue("@contraseña", newPassword1);
                            updateCommand.Parameters.AddWithValue("@usuario", _userViewModel.userName);
                            int rowsAffected = updateCommand.ExecuteNonQuery();

                            if (rowsAffected > 0)
                            {
                                // Si se ha actualizado la contraseña, muestra un mensaje de éxito y vuelve a la página de configuración
                                MessageBox.Show("Contraseña cambiada con éxito.");
                                var configPage = new ConfigPage(_userViewModel);
                                NavigationService.Navigate(configPage);
                            }
                            else
                            {
                                // Si no se ha actualizado la contraseña, muestra un mensaje de error
                                apLbl.Content = "";
                                npwLbl.Content = "";
                                npw2Lbl.Content = "";
                                errorMessageTextBlock.Text = "No se ha podido cambiar la contraseña.";
                            }
                        }
                        else
                        {
                            // Si la contraseña es incorrecta, muestra un mensaje de error
                            apLbl.Content = "";
                            npwLbl.Content = "";
                            npw2Lbl.Content = "";
                            errorMessageTextBlock.Text = "La contraseña actual es incorrecta.";
                        }
                    }
                    else
                    {
                        // Si no se encuentra al usuario, muestra un mensaje de error
                        apLbl.Content = "";
                        npwLbl.Content = "";
                        npw2Lbl.Content = "";
                        errorMessageTextBlock.Text = "No se ha encontrado al usuario.";
                    }

                    reader.Close();
                    connection.Close();
                }
            }
        }


        #endregion

        #region backButton_Click
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            var configPage = new ConfigPage(_userViewModel);
            NavigationService.Navigate(configPage);
        }
        #endregion
    }
}
