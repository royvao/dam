﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Battleship_WPF
{
    public partial class StatsPage : Page
    {
        #region components
        private readonly UserViewModel _userViewModel;
        string connectionString = @"Data Source=(local);Initial Catalog=Usuarios;Integrated Security=True";
        // string queryString = "SELECT tiempo, dificultad, minas FROM estadisticas WHERE nombre = @usuario";
        string queryString = "SELECT dificultad, minas, tiempo, fecha FROM estadisticas WHERE nombre = @usuario ORDER BY fecha DESC";
        #endregion
        public StatsPage(UserViewModel userViewModel)
        {
            #region info
            InitializeComponent();
            Loaded += StatsPage_Loaded;
            _userViewModel = userViewModel;
            CheckStats();
            #endregion
        }

        #region statsPage_Loaded
        private void StatsPage_Loaded(object sender, RoutedEventArgs e)
        {
            Window parentWindow = Window.GetWindow(this);
            parentWindow.Width = 500;
            parentWindow.Height = 600;
        }
        #endregion

        #region checkStats
        private void CheckStats()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@usuario", _userViewModel.userName);

                connection.Open();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    bool foundStats = false;

                    while (reader.Read())
                    {
                        // Recuperar los valores de las columnas "tiempo", "dificultad", "fecha" y "minas" de la fila actual
                        DateTime fecha = reader.GetDateTime(3);
                        double tiempoDouble = reader.GetDouble(2);
                        string tiempo = tiempoDouble.ToString();
                        string dificultad = reader.GetString(0);
                        int minas = reader.GetInt32(1);

                        // Crear nuevos Labels para cada estadística
                        Label dateLabel = new Label();
                        dateLabel.Content = "Fecha: " + fecha.ToShortDateString();
                        statsGrid.Children.Add(dateLabel);


                        Label timeLabel = new Label();
                        timeLabel.Content = "Tiempo: " + tiempo + " segundos";
                        statsGrid.Children.Add(timeLabel);

                        Label difLabel = new Label();
                        difLabel.Content = "Dificultad: " + dificultad;
                        statsGrid.Children.Add(difLabel);

                        Label minesLabel = new Label();
                        minesLabel.Content = "Minas: " + minas.ToString();
                        statsGrid.Children.Add(minesLabel);

                        // Agregar un Separator después de cada estadística
                        Separator separator = new Separator();
                        separator.Margin = new Thickness(0, 10, 0, 10);
                        statsGrid.Children.Add(separator);

                        foundStats = true;
                    }

                    if (!foundStats)
                    {
                        error.Content = "No se han encontrado estadísticas.";
                    }
                }
            }
        }
        #endregion

        #region exitButton_Click
        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            var menuPage = new MenuPage(_userViewModel);
            NavigationService.Navigate(menuPage);
        }
        #endregion

        #region clearButton_Click
        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {

        }

        #endregion

    }
}
