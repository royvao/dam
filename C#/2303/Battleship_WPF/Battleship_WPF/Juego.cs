﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship_WPF
{
    public class Juego
    {
        private GameBoard? tablero;
        private List<Ship>? ships;

        public void IniciarJuego()
        {
            tablero = new GameBoard(10); // Se proporciona el tamaño del tablero
            ships = new List<Ship>
            {
                // Agregamos los barcos al juego
                new Ship("Portaaviones", 5),
                new Ship("Acorazado", 4),
                new Ship("Destructor", 3),
                new Ship("Submarino", 3),
                new Ship("Fragata", 2)
            };

            // Colocamos los barcos en el tablero
            foreach (Ship ship in ships)
            {
                bool barcoColocado = false;

                while (!barcoColocado)
                {
                    // Generamos una posición aleatoria para el barco
                    Random rnd = new Random();
                    int filaInicial = rnd.Next(1, 11);
                    int columnaInicial = rnd.Next(1, 11);
                    Direccion direccion = (Direccion)rnd.Next(2);

                    // Intentamos colocar el barco en esa posición
                    if (tablero.PlaceShip(ship, filaInicial, columnaInicial, direccion))
                    {
                        barcoColocado = true;
                    }
                }
            }
        }

        public void Disparar(int row, int col)
        {
            // Obtenemos el resultado del disparo en la posición indicada
            GameBoard.ResultadoDisparo resultado = tablero.FireShot(row, col);

            switch (resultado)
            {
                case GameBoard.ResultadoDisparo.Agua:
                    Console.WriteLine("Agua");
                    break;
                case GameBoard.ResultadoDisparo.Tocado:
                    Console.WriteLine("Tocado");
                    break;
                case GameBoard.ResultadoDisparo.Hundido:
                    Console.WriteLine("Hundido");
                    break;
            }
        }
    }
}
