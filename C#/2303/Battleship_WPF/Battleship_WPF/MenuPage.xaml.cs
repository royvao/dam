﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Battleship_WPF
{
    /// <summary>
    /// Lógica de interacción para MenuPage.xaml
    /// </summary>
    public partial class MenuPage : Page
    {
        #region components
        private readonly UserViewModel _userViewModel;
        #endregion
        public MenuPage(UserViewModel userViewModel)
        {
            #region info
            InitializeComponent();
            Loaded += MenuPage_Loaded;
            _userViewModel = userViewModel;
            Welcome.Content = "¡Bienvenido, @" + _userViewModel.userName + "!";
            #endregion
        }

        #region menuPage_Loaded
        private void MenuPage_Loaded(object sender, RoutedEventArgs e)
        {
            Window parentWindow = Window.GetWindow(this);
            parentWindow.Width = 500;
            parentWindow.Height = 600;
        }
        #endregion

        #region playButton_Click
        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            var gamePage = new GamePage(_userViewModel);
            NavigationService.Navigate(gamePage);
            //NavigationService.Navigate(new GamePage());

        }
        #endregion

        #region statsButton_Click
        private void StatsButton_Click(object sender, RoutedEventArgs e)
        {
            var statsPage = new StatsPage(_userViewModel);
            NavigationService.Navigate(statsPage);
        }
        #endregion

        #region exitButton_Click
        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new LoginPage());

        }
        #endregion

        #region configButton_Click
        private void ConfigButton_Click(object sender, RoutedEventArgs e)
        {
            var configPage = new ConfigPage(_userViewModel);
            NavigationService.Navigate(configPage);
        }
        #endregion
    }
}
