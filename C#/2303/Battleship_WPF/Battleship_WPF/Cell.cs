﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship_WPF
{
    public class Cell
    {
        public int Row { get; set; }
        public int Column { get; set; }
        public bool IsEmpty { get; set; }
        public bool IsHit { get; set; }
        public Ship Ship { get; set; } // propiedad para almacenar el barco en esta celda

        public bool HasShip => !IsEmpty && Ship != null;
    }
}
