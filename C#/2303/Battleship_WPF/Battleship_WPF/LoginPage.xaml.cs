﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Data.SqlClient;
using System.Windows.Navigation;

#region quitAdverts
#pragma warning disable	CS8618
#pragma warning disable	CS8600
#endregion


namespace Battleship_WPF
{
    /// <summary>
    /// Lógica de interacción para LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        #region components
        string connectionString = @"Data Source=(local);Initial Catalog=Usuarios;Integrated Security=True";
        string queryString = "SELECT COUNT(*) FROM usersData WHERE userName = @usuario AND userPassword = @contraseña";
        #endregion
        public LoginPage()
        {
            #region info
            InitializeComponent();
            Loaded += LoginPage_Loaded;
            #endregion        
        }

    #region loginPage_Loaded
    private void LoginPage_Loaded(object sender, RoutedEventArgs e)
    {
        Window parentWindow = Window.GetWindow(this);
        parentWindow.Width = 350;
        parentWindow.Height = 400;

        // Obtener la instancia de la ventana principal
        MainWindow mainWindow = Application.Current.MainWindow as MainWindow;
        if (mainWindow != null)
        {
            // Restablecer el estilo de la ventana principal
            mainWindow.WindowStyle = WindowStyle.SingleBorderWindow;
            mainWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }
    }
    #endregion

    #region loginButton_Click
    private void LoginButton_Click(object sender, RoutedEventArgs e)
    {
        var userViewModel = new UserViewModel();
        userViewModel.userName = usernameTextBox.Text;
        string password = passwordBox.Password;

        // Realizar la consulta a la base de datos al momento de crear la ventana
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            SqlCommand command = new SqlCommand(queryString, connection);
            command.Parameters.AddWithValue("@usuario", userViewModel.userName);
            command.Parameters.AddWithValue("@contraseña", password);
            connection.Open();
            int count = (int)command.ExecuteScalar();
            if (count == 1)
            {
                // El usuario y la contraseña son válidos                    
                var menuPage = new MenuPage(userViewModel);
                NavigationService.Navigate(menuPage);

            }
            else
            {
                // El usuario y/o la contraseña son incorrectos
                errorMessageTextBlock.Text = "Usuario o contraseña incorrectos.";
            }
        }
    }
    #endregion

    private void Button_Click(object sender, RoutedEventArgs e)
    {
        NavigationService.Navigate(new SignupPage());
    }
}
}
