﻿using System.Net.Sockets;
using System.Net;
using System.Text;

namespace Servidor_Sockets
{
    class Program
    {
        static void Main(string[] args)
        {
           IPEndPoint connect = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 6400);

            try
            {
                Socket listen = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                listen.Bind(connect);
                listen.Listen(10);
                Console.WriteLine("Esperando conexión...");

                Socket conexion = listen.Accept();
                Console.WriteLine("Conexión aceptada.");

                while (true)
                {
                    string data = "";
                    int array_size = 0;

                    while (true)
                    {
                        byte[] recibir_info = new byte[1024];

                        array_size = conexion.Receive(recibir_info, 0, recibir_info.Length, 0);
                        Array.Resize(ref recibir_info, array_size);

                        data = Encoding.Default.GetString(recibir_info);
                        if (data.IndexOf("<EOF>") > -1)
                            break;
                    }

                    Console.WriteLine(data.Replace("<EOF>", ""));
                    byte[] msg = Encoding.Default.GetBytes(data);
                    conexion.Send(msg);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}      

