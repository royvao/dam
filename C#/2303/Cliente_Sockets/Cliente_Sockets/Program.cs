﻿using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Cliente_Sockets
{
    class Program
    {
        static void Main(string[] args)
        {
            IPEndPoint connect = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 6400);

            try
            {
                Socket listen = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                listen.Connect(connect);
                Console.WriteLine("Conectando al servidor...");
                string data = "";

                while (data != "exit")
                {
                    Console.WriteLine("Ingrese la información a enviar: ");
                    data = Console.ReadLine();
                    byte[] msg = Encoding.ASCII.GetBytes(data+"<EOF>");
                    int byteSent = listen.Send(msg);
                    byte[] enviar_info = new byte[1024];
                    int byteRec = listen.Receive(enviar_info);
                    
                }
                listen.Shutdown(SocketShutdown.Both);
                listen.Close();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            

        }

    }
}