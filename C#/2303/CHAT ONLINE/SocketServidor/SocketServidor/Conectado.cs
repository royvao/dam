﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace SocketServidor
{
    internal class Conectado
    {
        private string nick;
        private Thread recibir;
        private Socket handler;
        private Dictionary<string, Conectado> di;
        private Conexion con;

        public Conectado(Dictionary<string, Conectado> di, Socket handler)
        {
            this.recibir = new Thread(receive);
            this.recibir.Start();
            this.di = di;
            this.handler = handler;
            this.con = new Conexion();
        }

        public void send(string mensaje)
        {
            this.con.send(mensaje, this.handler);
        }

        public void sendUser(string[] todo)
        {
            try
            {
                Conectado co = this.di[todo[0]];
                co.send(this.nick + "-" + todo[1]);
            }
            catch (KeyNotFoundException ke)
            {
                Console.WriteLine("Key:" + todo[0]);
                Console.WriteLine(ke.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void receive()
        {
            while (true)
            {
                if(this.nick == null)
                {
                    this.nick = this.con.receive(this.handler);
                    di.Add(this.nick, this);
                    this.send("conectado");
                }
                else
                {
                    string msg = this.con.receive(this.handler);
                    if(msg == "desconectar")
                    {
                        di.Remove(this.nick);
                        Conectado co = this;
                        co.handler.Shutdown(SocketShutdown.Both);
                        co.handler.Close();
                        co = null;
                    }
                    else
                    {
                        Console.WriteLine(this.nick + ": " +msg);
                        this.sendUser(msg.Split('-'));
                    }
                }
            }
        }

        public string Nick { get => nick; set => nick = value; }
    }
}
