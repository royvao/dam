﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketServidor
{
    internal class Conexion
    {
        IPHostEntry host;
        IPAddress ipAddress;
        IPEndPoint localEP;
        Socket listener;

        public Conexion()
        {

        }

        public void connect()
        {
            this.host = Dns.GetHostEntry("localhost");
            this.ipAddress = host.AddressList[0];
            this.localEP = new IPEndPoint(ipAddress, 13500);

            try
            {
                this.listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                this.listener.Bind(localEP);
                this.listener.Listen(10);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public string receive(Socket handler)
        {
            try
            {
                string data = null;
                byte[] bytes = null;
                while (true)
                {
                    bytes = new byte[1024];
                    int byteRec = handler.Receive(bytes);
                    data += Encoding.ASCII.GetString(bytes, 0, byteRec);
                    if (data.IndexOf("<EOF>") > -1)
                        break;
                }
                    data = data.Replace("<EOF>", "");
                    return data;
                
            }
            catch (Exception ex) { Console.WriteLine(); return ""; }
        }

        public void send(string mensaje, Socket handler)
        {
            try
            {
                byte[] msg = Encoding.ASCII.GetBytes(mensaje+"<EOF>");
                int byteSent = handler.Send(msg);
            }
            catch (Exception ex) { Console.WriteLine(); }
        }

        public Socket conectado()
        {
            return this.listener.Accept();
        }
    }
}
