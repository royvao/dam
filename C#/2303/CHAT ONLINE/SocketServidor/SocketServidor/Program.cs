﻿using System.Net.Sockets;
using System.Net;


namespace SocketServidor
{
    class Program
    {
        static Dictionary<string, Conectado> di = new Dictionary<string, Conectado>();
        static void Main(string[] args)
        {
            Conexion con = new Conexion();
            con.connect();
            while (true)
            {
                Socket handler = con.conectado();
                Conectado c = new Conectado(di, handler);
            }
        }
    }
}
