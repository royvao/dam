﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace SocketCliente
{
    public class Conexion
    {
        IPHostEntry host;
        IPAddress ipAddress;
        IPEndPoint localEP;
        Socket enviar;

        public Conexion()
        {

        }

        public void conectar()
        {
            host = Dns.GetHostEntry("localhost");
            ipAddress = host.AddressList[0];
            localEP = new IPEndPoint(ipAddress, 13500);
            enviar = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            enviar.Connect(localEP);
        }

        public void send(string mensaje)
        {
            try
            {
                byte[] msg = Encoding.ASCII.GetBytes(mensaje+"<EOF>");
                enviar.Send(msg);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public string receive()
        {
            try
            {
                string data = null;
                byte[] bytes = null;
                while(true)
                {
                    bytes = new byte[1024];
                    int byteRec = this.enviar.Receive(bytes);
                    data += Encoding.ASCII.GetString(bytes, 0, byteRec);
                    if (data.IndexOf("<EOF>") > -1)
                        break;
                }
                data = data.Replace("<EOF>", "");
                return data;
            }
            catch (Exception ex) { Console.WriteLine(); return ""; }
        }

        public void desconectar()
        {
            enviar.Shutdown(SocketShutdown.Both);
            enviar.Close();
        }
    }
}
