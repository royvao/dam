﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SocketCliente
{
    public partial class Form1 : Form
    {
        private Thread rec;
        public Dictionary<string, Chat> di = new Dictionary<string, Chat>();
        public Conexion con;

        public Form1()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
            con = new Conexion();
            con.conectar();
            rec = new Thread(recibir);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
            //https://www.youtube.com/watch?v=-W5QKYPV83w
        }

        private void btnConectar_Click(object sender, EventArgs e)
        {
            if(txtNick.Text != "")
            {
                con.send(txtNick.Text);
                string msg = con.receive();
                if (msg == "conectado")
                {
                    Seleccion s = new Seleccion(txtNick.Text, di, con);
                    s.Show();
                    rec.Start();
                }
                else
                {
                    MessageBox.Show("Error de conexión");
                }
            }
            else
            {
                MessageBox.Show("Escriba su nick");
            }
        }

        private void recibir()
        {
            while (true)
            {
                string msg = con.receive();
                string[] todo = msg.Split('-');
                Chat c = di[todo[0]];
                c.agregar(todo[0] + ": " + todo[1]);
            }
        }



        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            con.send("desconectar");
            con.desconectar();
        }

    }
}
