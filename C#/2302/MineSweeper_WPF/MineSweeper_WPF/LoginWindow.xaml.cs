﻿using System;
using System.Windows;
using System.Data.SqlClient;
using System.Reflection.PortableExecutable;

#region quitAdverts
#pragma warning disable	CS8618
#endregion

namespace MineSweeper_WPF
{
    /// <summary>
    /// Lógica de interacción para Window1.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public string Username;
        string connectionString = @"Data Source=(local);Initial Catalog=Usuarios;Integrated Security=True";
        string queryString = "SELECT COUNT(*) FROM usersData WHERE userName = @usuario AND userPassword = @contraseña";
      
        public LoginWindow()
        {
            InitializeComponent();
            
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            string username = usernameTextBox.Text;
            string password = passwordBox.Password;

            // Realizar la consulta a la base de datos al momento de crear la ventana
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@usuario", username);
                command.Parameters.AddWithValue("@contraseña", password);
                connection.Open();
                int count = (int)command.ExecuteScalar();
                if (count == 1)
                {
                    // El usuario y la contraseña son válidos
                    // Establecer DialogResult en true para indicar que se inició sesión correctamente
                    DialogResult = true;
                // Abrir la ventana principal del juego
                

                    // Si las credenciales son válidas, establecer el nombre de usuario y cerrar la ventana de inicio de sesión
                    Username = usernameTextBox.Text;
                    Close();
                }
                else
                {
                    // El usuario y/o la contraseña son incorrectos
                    errorMessageTextBlock.Text = "Usuario o contraseña incorrectos.";
                }
            }
        }

    }
}
