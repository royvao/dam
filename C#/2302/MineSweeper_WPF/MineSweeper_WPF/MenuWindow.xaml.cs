﻿using System;
using System.Windows;

namespace MineSweeper_WPF
{
    /// <summary>
    /// Lógica de interacción para Window1.xaml
    /// </summary>
    public partial class MenuWindow : Window
    {
        LoginWindow loginWindow = new();

        public MenuWindow()
        {
            InitializeComponent();
        }


        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void StatsButton_Click(object sender, RoutedEventArgs e)
        {
            StatsFrame.NavigationService.Navigate(new StatsPage());
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
            loginWindow.Show();
        }
    }
}
