﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

#region quitAdverts
#pragma warning disable	CS8622
#endregion


namespace MineSweeper2._0
{
    public partial class HomePage : Page
    {
        #region components
        private DispatcherTimer timer;
        private const int MAX_PROGRESS_VALUE = 100;

        #endregion

        public HomePage()
        {
            #region info
            InitializeComponent();

            // Configurar el temporizador para que actualice la barra de progreso cada 50ms
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(50);
            timer.Tick += Timer_Tick;
            timer.Start();
            Loaded += HomePage_Loaded;
            
            #endregion
        }

        #region homePage_Loaded
        private void HomePage_Loaded(object sender, RoutedEventArgs e)
        {
            // Obtener la instancia de la ventana principal
            MainWindow mainWindow = Application.Current.MainWindow as MainWindow;
            if (mainWindow != null)
            {
                // Establecer el estilo de la ventana principal en None
                mainWindow.WindowStyle = WindowStyle.None;
            }
        }
        #endregion

        #region timer_Tick
        private void Timer_Tick(object sender, EventArgs e)
        {
            // Incrementar la barra de progreso hasta el valor máximo
            loadingBar.Value += 1;

            // Si la barra de progreso ha alcanzado el valor máximo, detener el temporizador y navegar a la siguiente página
            if (loadingBar.Value == MAX_PROGRESS_VALUE)
            {
                timer.Stop();

                // Navegar a la siguiente página
                NavigationService.Navigate(new LoginPage());

            }
        }
        #endregion
    }
}
