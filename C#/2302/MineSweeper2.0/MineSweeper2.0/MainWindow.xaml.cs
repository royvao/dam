﻿using System;
using System.Windows;

namespace MineSweeper2._0
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            #region mainInfo
            InitializeComponent();

            // Crea un nuevo objeto Frame y agrega la página de inicio
            MainFrame.Navigate(new Uri("HomePage.xaml", UriKind.Relative));
            #endregion
        }
    }
}
