﻿using Microsoft.AspNetCore.Mvc;
using MVC_Catálogo_Imágenes.Data;
using MVC_Catálogo_Imágenes.Models;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Linq.Expressions;

namespace MVC_Catálogo_Imágenes.Controllers
{
    public class CuentaController : Controller
    {
        private readonly DbContext _contexto;
        public static bool IsAuth { get; private set; }
        public static bool IsVerify { get; private set; }
        public CuentaController(DbContext contexto)
        {
            _contexto = contexto;
        }

        public ActionResult Registrarse()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Registrarse(UsuarioModel u)
        {
            try
            {
                using (SqlConnection con = new(_contexto.Valor))
                {
                    using (SqlCommand cmd = new("sp_registrar", con))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.Add("@Nombre", System.Data.SqlDbType.VarChar).Value = u.Nombre;
                        cmd.Parameters.Add("@Apellido", System.Data.SqlDbType.VarChar).Value = u.Apellido;
                        cmd.Parameters.Add("@Fecha_Nacimiento", System.Data.SqlDbType.Date).Value = u.Fecha_Nacimiento;
                        cmd.Parameters.Add("@Correo", System.Data.SqlDbType.VarChar).Value = u.Correo;
                        cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = u.User;
                        cmd.Parameters.Add("@Pass", System.Data.SqlDbType.VarChar).Value = u.Password;
                        var token = Guid.NewGuid();
                        cmd.Parameters.Add("@Token", System.Data.SqlDbType.VarChar).Value = token.ToString();
                        cmd.Parameters.Add("@Estado", System.Data.SqlDbType.Bit).Value = 0;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        Email email = new();
                        if (u.Correo != null)
                        {
                            Response.Cookies.Append("mail", u.Correo);
                            Response.Cookies.Append("user", u.Nombre);
                            email.Enviar(u.Correo, token.ToString());
                        }
                        con.Close();
                    }
                }
                return RedirectToAction("Token", "Cuenta");

            }
            catch (Exception)
            {
                ViewData["error"] = "Ha habido un error en el registro";
                return View();
            }
        }

        public ActionResult Token()
        {
            var model = new UsuarioModel
            {
                Correo = Request.Cookies["mail"],
                Nombre = Request.Cookies["user"]
            };
            string token = Request.Query["valor"];

            if (token != null)
            {
                try
                {
                    using (SqlConnection con = new(_contexto.Valor))
                    {
                        using (SqlCommand cmd = new("sp_validar", con))
                        {
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.Add("@Token", System.Data.SqlDbType.VarChar).Value = token;
                            con.Open();
                            cmd.ExecuteNonQuery();
                            ViewData["mensaje"] = "Su cuenta se ha validado con éxito.";
                            IsVerify = true;
                            con.Close();
                        }
                    }
                    return View(model);
                }
                catch (Exception e)
                {
                    ViewData["mensaje"] = e.Message;
                    return View(model);

                }
            }
            else
            {
                ViewData["mensaje"] = "Verificar su correo para activar la cuenta.";                
                return View(model);
            }
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel loginModel)
        {
            try
            {
                using (SqlConnection con = new(_contexto.Valor))
                {
                    using (SqlCommand cmd = new("sp_login", con))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = loginModel.User;
                        cmd.Parameters.Add("@Pass", System.Data.SqlDbType.VarChar).Value = loginModel.Password;
                        cmd.Parameters.Add("@Estado", System.Data.SqlDbType.Bit).Value = 1;
                        con.Open();
                        object result = cmd.ExecuteScalar();
                        int count = result == null ? 0 : Convert.ToInt32(result);                        
                        con.Close();
                        if (count > 0)
                        {
                            IsAuth = true;
                            Response.Cookies.Append("un", loginModel.User);
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            ViewData["error"] = "Usuario y/o contraseña incorrectos";
                            return View(loginModel.User);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                ViewData["error2"] = "Ha habido un error al iniciar sesión" + e.Message;
                return View();
            }
        }

        public ActionResult Logout()
        {
            IsAuth = false;
            Response.Cookies.Delete("un");
            Response.Cookies.Delete("mail");
            Response.Cookies.Delete("user");
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Profile()
        {
            var model = new LoginModel
            {
                User = Request.Cookies["un"]
            };
            return View(model);
        }
    }
}
