﻿using Microsoft.AspNetCore.Mvc;
using MVC_Catálogo_Imágenes.Data;
using MVC_Catálogo_Imágenes.Models;
using System.Data.SqlClient;
using System.Data;

namespace MVC_Catálogo_Imágenes.Controllers
{
    public class ImagenController : Controller
    {
        private readonly DbContext _contexto;

        public ImagenController(DbContext contexto)
        {
            _contexto = contexto;
        }

        [HttpGet]
        public IActionResult Catalogo()
        {
            try
            {
                using (SqlConnection con = new(_contexto.Valor))
                {
                    List<ImagenModel> lista_imagenes = new();
                    using (SqlCommand cmd = new("sp_Obtener_Imagenes", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        con.Open();

                        var dr = cmd.ExecuteReader();

                        while (dr.Read())
                        {
                            lista_imagenes.Add(new ImagenModel
                            {
                                Id_Imagen = (int)dr["Id_Imagen"],
                                Nombre = dr["Nombre"].ToString(),
                                Imagen = dr["Imagen"].ToString(),
                                Fuente = dr["Fuente"].ToString()
                            });
                        }
                        //con.Close();
                    }
                    ViewBag.Cat = lista_imagenes;
                    return View();
                }
            }
            catch (Exception)
            {
                ViewData["error"] = "Ha habido un error al cargar las imágenes";
                return View();
            }
        }

        public IActionResult EditCatalog()
        {
            return View();
        }

        [HttpPost]
        public IActionResult EditCatalog(ImagenModel i)
        {
            try
            {
                using (SqlConnection con = new(_contexto.Valor))
                {
                    using (SqlCommand cmd = new("sp_Añadir_Imagenes", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = i.Nombre;
                        cmd.Parameters.Add("@Imagen", SqlDbType.VarChar).Value = i.Imagen;
                        cmd.Parameters.Add("@Fuente", SqlDbType.VarChar).Value = i.Fuente;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
                return RedirectToAction("Catalogo", "Imagen");
            }
            catch (Exception)
            {
                return View();
            }            
        }
    }
}
