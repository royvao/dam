﻿insert into Imagenes values(
'Developer at office - Style CGI',
'https://images.nightcafe.studio/jobs/KrmD3P6difRHhvUOVES1/KrmD3P6difRHhvUOVES1--1--j7u8u.jpg?tr=w-1600,c-at_max',
'https://creator.nightcafe.studio/studio'
)
insert into Imagenes values(
'Developer at office - Style 3D Game',
'https://images.nightcafe.studio/jobs/tAdEAboC0uapqTTnqGmB/tAdEAboC0uapqTTnqGmB--1--b23m0.jpg?tr=w-1600,c-at_max',
'https://creator.nightcafe.studio/studio'
)
insert into Imagenes values(
'Developer at office - Style Artistic Portrait',
'https://images.nightcafe.studio/jobs/0vbVjCseJ3Ar3GbtZd3L/0vbVjCseJ3Ar3GbtZd3L--1--42b03.jpg?tr=w-1600,c-at_max',
'https://creator.nightcafe.studio/studio'
)
insert into Imagenes values(
'Developer at office - Style Epic',
'https://images.nightcafe.studio/jobs/ZthR2BUFahTYirV6MrdK/ZthR2BUFahTYirV6MrdK--1--vw0sq.jpg?tr=w-1600,c-at_max',
'https://creator.nightcafe.studio/studio'
)
insert into Imagenes values(
'Developer at office - Style Modern Comic',
'https://images.nightcafe.studio/jobs/KaI7mQKqsKnFFtpi68xu/KaI7mQKqsKnFFtpi68xu--1--p9r1v.jpg?tr=w-1600,c-at_max',
'https://creator.nightcafe.studio/studio'
)
insert into Imagenes values(
'Developer at office - Style Anime',
'https://images.nightcafe.studio/jobs/bOLZUDBSQjcjtj5BNbgW/bOLZUDBSQjcjtj5BNbgW--1--672sd.jpg?tr=w-1600,c-at_max',
'https://creator.nightcafe.studio/studio'
)
insert into Imagenes values(
'Developer at office - Style Neo Impressionist',
'https://images.nightcafe.studio/jobs/MBq3AWHfsjsXsrRmbqTF/MBq3AWHfsjsXsrRmbqTF--1--yn92w.jpg?tr=w-1600,c-at_max',
'https://creator.nightcafe.studio/studio'
)
insert into Imagenes values(
'Developer at office - Style Horror',
'https://images.nightcafe.studio/jobs/H5PlXKCvLBiE7JKPFOzu/H5PlXKCvLBiE7JKPFOzu--1--b5ch0.jpg?tr=w-1600,c-at_max',
'https://creator.nightcafe.studio/studio'
)
insert into Imagenes values(
'Developer at office - Style Heavenly',
'https://images.nightcafe.studio/jobs/O7KrPTBZtNUNuN0ihvHu/O7KrPTBZtNUNuN0ihvHu--1--39l1h.jpg?tr=w-1600,c-at_max',
'https://creator.nightcafe.studio/studio'
)
insert into Imagenes values(
'Developer at office - Style Surreal',
'https://images.nightcafe.studio/jobs/LY4iLk6sLOyzCmvXERSB/LY4iLk6sLOyzCmvXERSB--1--vxbop.jpg?tr=w-1600,c-at_max',
'https://creator.nightcafe.studio/studio'
)
insert into Imagenes values(
'Developer at office - Style Dark Fantasy',
'https://images.nightcafe.studio/jobs/80Q0lRo5bLXxu24wGPNn/80Q0lRo5bLXxu24wGPNn--1--ktd2t.jpg?tr=w-1600,c-at_max',
'https://creator.nightcafe.studio/studio'
)
insert into Imagenes values(
'Developer at office - Style Bon Voyage',
'https://images.nightcafe.studio/jobs/376Un35xXtbjyvw889DI/376Un35xXtbjyvw889DI--1--h9ihf.jpg?tr=w-1600,c-at_max',
'https://creator.nightcafe.studio/studio'
)