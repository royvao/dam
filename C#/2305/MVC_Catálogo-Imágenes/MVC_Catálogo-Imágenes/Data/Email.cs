﻿using System.Net.Mail;
using System.Net;

namespace MVC_Catálogo_Imágenes.Data
{
    public class Email
    {
        public void Enviar(string correo, string token)
        {
            Correo(correo, token);
        }

        void Correo(string correo_receptor, string token)
        {
            string correo_emisor = "{tu correo}";
            string clave_emisor = "{tu contraseña}";

            MailAddress receptor = new(correo_receptor);
            MailAddress emisor = new(correo_emisor);

            MailMessage email = new(emisor, receptor)
            {
                Subject = "Validación de cuenta",
                Body = "Para activar su cuenta haga clic en el siguiente enlace: https://asproy.azurewebsites.net/Cuenta/Token?valor=" + token
            };

            SmtpClient smtp = new()
            {
                Host = "smtp.office365.com",
                Port = 587,
                Credentials = new NetworkCredential(correo_emisor, clave_emisor),
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = true
            };

            try
            {
                smtp.Send(email);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
