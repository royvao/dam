﻿using System.ComponentModel.DataAnnotations;

namespace MVC_Catálogo_Imágenes.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Campo obligatorio")]
        public string? User { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        public string? Password { get; set; }

    }
}
