﻿create table Usuarios(
Id_Usuario int identity(1,1) primary key,
Nombre varchar(100),
Apellido varchar(100),
Fecha_Nacimiento date,
Correo varchar(100),
Usuario varchar(100),
Pass varchar(100),
Token varchar(100),
Estado bit
)

create procedure sp_registrar
@Nombre varchar(100),
@Apellido varchar(100),
@Fecha_Nacimiento date,
@Correo varchar(100),
@Usuario varchar(100),
@Pass varchar(100),
@Token varchar(100),
@Estado bit
as begin
INSERT INTO Usuarios values(@Nombre,@Apellido,@Fecha_Nacimiento,@Correo,@Usuario,@Pass,@Token,@Estado)
end

CREATE PROCEDURE sp_validar
@Token varchar(max)
AS BEGIN
DECLARE @Correo varchar(100)
set @Correo = (select Correo from Usuarios where Token=@Token)
update Usuarios set Estado=1 where Token=@Token
update Usuarios set Token=null where Correo=@Correo
END

CREATE procedure sp_login
@Usuario varchar(50),
@Pass varchar(50),
@Estado bit
as begin
select * from Usuarios where Usuario=@Usuario and Pass=@Pass and Estado=1
end

create table Imagenes(
Id_Imagen int identity(1,1) not null primary key,
Nombre varchar(100),
Imagen varchar(max),
Fuente varchar(max)
)

create procedure sp_Obtener_Imagenes
as begin
select * from Imagenes
end

create procedure sp_Añadir_Imagenes
@Nombre varchar(100),
@Imagen varchar(max),
@Fuente varchar(max)
as begin
insert into Imagenes values (@Nombre, @Imagen, @Fuente)
end