﻿using CapaDatos;
using CapaEntidad;
using System.Collections.Generic;

namespace CapaNegocio
{
    public class CN_Usuarios
    {
        private CD_Usuarios oCapaDato = new CD_Usuarios();
        public List<Usuario> Listar()
        {
            return oCapaDato.Listar();
        }
        public int Registrar(Usuario u, out string Mensaje)
        {
            Mensaje = string.Empty;
            if(string.IsNullOrEmpty(u.Nombres) || string.IsNullOrWhiteSpace(u.Nombres))
            {
                Mensaje = "El nombre del usuario no puede estar vacío";
            }
            else if (string.IsNullOrEmpty(u.Apellidos) || string.IsNullOrWhiteSpace(u.Apellidos))
            {
                Mensaje = "Los apellidos del usuario no pueden estar vacíos";
            }
            else if (string.IsNullOrEmpty(u.Correo) || string.IsNullOrWhiteSpace(u.Correo))
            {
                Mensaje = "El correo del usuario no puede estar vacío";
            }

            if (string.IsNullOrEmpty(Mensaje))
            {
                string clave = CN_Recursos.GenerarClave();
                string asunto = "Creación de cuenta";
                string cuerpo = "<h3>Su cuenta fue creada correctamente.</h3><br><p>Su contraseña para acceder es: !clave!</p>";
                cuerpo = cuerpo.Replace("!clave!",clave);

                bool respuesta = CN_Recursos.EnviarCorreo(u.Correo, asunto, cuerpo);

                if (respuesta)
                {
                    u.Clave = CN_Recursos.ConvertirSha256(clave);
                    return oCapaDato.Registrar(u, out Mensaje);
                }
                else
                {
                    Mensaje = "No se ha podido enviar el correo";
                    return 0;
                }
            }
            else
            {
                return 0;
            }

        }

        public bool Editar(Usuario u, out string Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(u.Nombres) || string.IsNullOrWhiteSpace(u.Nombres))
            {
                Mensaje = "El nombre del usuario no puede estar vacío";
            }
            else if (string.IsNullOrEmpty(u.Apellidos) || string.IsNullOrWhiteSpace(u.Apellidos))
            {
                Mensaje = "Los apellidos del usuario no pueden estar vacíos";
            }
            else if (string.IsNullOrEmpty(u.Correo) || string.IsNullOrWhiteSpace(u.Correo))
            {
                Mensaje = "El correo del usuario no puede estar vacío";
            }

            if(string.IsNullOrEmpty(Mensaje))
            {
                return oCapaDato.Editar(u, out Mensaje);
            }
            else
            {
                return false;
            }
        }

        public bool Eliminar(int id, out string Mensaje)
        {
            return oCapaDato.Eliminar(id, out Mensaje);
        }
    }
}
