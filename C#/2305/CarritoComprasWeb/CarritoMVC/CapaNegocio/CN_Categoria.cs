﻿using CapaDatos;
using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class CN_Categoria
    {
        private CD_Categoria oCapaDato = new CD_Categoria();
        public List<Categoria> Listar()
        {
            return oCapaDato.Listar();
        }

        public int Registrar(Categoria c, out string Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(c.Descripcion) || string.IsNullOrWhiteSpace(c.Descripcion))
            {
                Mensaje = "La descripción no puede estar vacía.";
            }

            if (string.IsNullOrEmpty(Mensaje))
            {
                return oCapaDato.Registrar(c, out Mensaje);                
            }
            else
            {
                return 0;
            }

        }

        public bool Editar(Categoria c, out string Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(c.Descripcion) || string.IsNullOrWhiteSpace(c.Descripcion))
            {
                Mensaje = "La descripción no puede estar vacía.";
            }

            if (string.IsNullOrEmpty(Mensaje))
            {
                return oCapaDato.Editar(c, out Mensaje);
            }
            else
            {
                return false;
            }
        }

        public bool Eliminar(int id, out string Mensaje)
        {
            return oCapaDato.Eliminar(id, out Mensaje);
        }
    }
}
