﻿using CapaDatos;
using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class CN_Marca
    {
        private CD_Marca oCapaDato = new CD_Marca();
        public List<Marca> Listar()
        {
            return oCapaDato.Listar();
        }

        public int Registrar(Marca m, out string Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(m.Descripcion) || string.IsNullOrWhiteSpace(m.Descripcion))
            {
                Mensaje = "La descripción no puede estar vacía.";
            }

            if (string.IsNullOrEmpty(Mensaje))
            {
                return oCapaDato.Registrar(m, out Mensaje);
            }
            else
            {
                return 0;
            }

        }

        public bool Editar(Marca m, out string Mensaje)
        {
            Mensaje = string.Empty;
            if (string.IsNullOrEmpty(m.Descripcion) || string.IsNullOrWhiteSpace(m.Descripcion))
            {
                Mensaje = "La descripción no puede estar vacía.";
            }

            if (string.IsNullOrEmpty(Mensaje))
            {
                return oCapaDato.Editar(m, out Mensaje);
            }
            else
            {
                return false;
            }
        }

        public bool Eliminar(int id, out string Mensaje)
        {
            return oCapaDato.Eliminar(id, out Mensaje);
        }
    }
}
