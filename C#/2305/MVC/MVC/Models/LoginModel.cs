﻿using System.ComponentModel.DataAnnotations;

namespace MVC.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Campo obligatorio")]
        public string? Usuario { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        public string? Clave { get; set; }
    }
}
