﻿using System.ComponentModel.DataAnnotations;

namespace MVC.Models
{
    public class UsuarioModel
    {
        [Key]
        public int Id_Usuario { get; set; }

        [Required (ErrorMessage = "Campo obligatorio")]
        public string? Nombre { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        public string? Correo { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        public int Edad { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        public string? Usuario { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        public string? Clave { get; set; }

    }
}
