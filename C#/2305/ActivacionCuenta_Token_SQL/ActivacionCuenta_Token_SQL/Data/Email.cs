﻿using System.Net;
using System.Net.Mail;

namespace ActivacionCuenta_Token_SQL.Data
{
    public class Email
    {
        public void Enviar(string correo, string token)
        {
            Correo(correo, token);
        }

        void Correo(string correo_receptor, string token)
        {
            string correo_emisor = "rdominguez@codisys.es";
            string clave_emisor = "KSI76YtgNjGE_ayjYOBn.x/@";

            MailAddress receptor = new(correo_receptor);
            MailAddress emisor = new(correo_emisor);

            MailMessage email = new(emisor, receptor)
            {
                Subject = "Validación de cuenta",
                Body = "Para activar su cuenta haga clic en el siguiente enlace: https://localhost:7098/Cuenta/Token?valor=" + token
            };

            SmtpClient smtp = new()
            {
                Host = "smtp.office365.com",
                Port = 587,
                Credentials = new NetworkCredential(correo_emisor, clave_emisor),
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = true
            };

            try
            {
                smtp.Send(email);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
