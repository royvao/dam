﻿using System.ComponentModel.DataAnnotations;

namespace ActivacionCuenta_Token_SQL.Models
{
    public class UsuarioModel
    {
        [Key]
        public int Id_Usuario { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        public string? Nombre { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        public string? Apellido { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        public DateTime Fecha_Nacimiento { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        public string? Correo { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        public string? User { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        public string? Password { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        public string? Token { get; set; }

        public byte Estado { get; set; }
    }
}
