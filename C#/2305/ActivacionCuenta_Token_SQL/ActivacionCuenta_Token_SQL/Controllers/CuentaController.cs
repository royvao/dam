﻿using ActivacionCuenta_Token_SQL.Data;
using ActivacionCuenta_Token_SQL.Models;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;

namespace ActivacionCuenta_Token_SQL.Controllers
{
    public class CuentaController : Controller
    {
        private readonly DbContext _contexto;

        public CuentaController(DbContext contexto)
        {
            _contexto = contexto;
        }

        public ActionResult Registrarse()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Registrarse(UsuarioModel u)
        {
            try
            {

                using (SqlConnection con = new(_contexto.Valor))
                {
                    using (SqlCommand cmd = new("sp_registrar", con))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.Add("@Nombre", System.Data.SqlDbType.VarChar).Value = u.Nombre;
                        cmd.Parameters.Add("@Apellido", System.Data.SqlDbType.VarChar).Value = u.Apellido;
                        cmd.Parameters.Add("@Fecha_Nacimiento", System.Data.SqlDbType.Date).Value = u.Fecha_Nacimiento;
                        cmd.Parameters.Add("@Correo", System.Data.SqlDbType.VarChar).Value = u.Correo;
                        cmd.Parameters.Add("@Usuario", System.Data.SqlDbType.VarChar).Value = u.User;
                        cmd.Parameters.Add("@Pass", System.Data.SqlDbType.VarChar).Value = u.Password;
                        var token = Guid.NewGuid();
                        cmd.Parameters.Add("@Token", System.Data.SqlDbType.VarChar).Value = token.ToString();
                        cmd.Parameters.Add("@Estado", System.Data.SqlDbType.Bit).Value = 0;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        Email email = new();
                        if (u.Correo != null)
                        {
                            email.Enviar(u.Correo, token.ToString());
                        }
                        con.Close();
                    }
                }
                return RedirectToAction("Token", "Cuenta");

            }
            catch (Exception e)
            {
                ViewData["error"] = e.Message;
                return View();
            }
            return View();
        }

        public ActionResult Token()
        {
            string token = Request.Query["valor"];

            if (token != null)
            {
                try
                {
                    using (SqlConnection con = new(_contexto.Valor))
                    {
                        using (SqlCommand cmd = new("sp_validar", con))
                        {
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.Add("@Token", System.Data.SqlDbType.VarChar).Value = token;
                            con.Open();
                            cmd.ExecuteNonQuery();
                            ViewData["mensaje"] = "Su cuenta se ha validado con éxito.";
                            con.Close();
                        }
                    }
                    return View();
                }
                catch (Exception e)
                {
                    ViewData["mensaje"] = e.Message;
                    return View();
                }
            }
            else
            {
                ViewData["mensaje"] = "Verifique su correo para activar su cuenta.";
                return View();
            }
        }
    }
}
