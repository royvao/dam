﻿using IntroASP.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IntroASP.Controllers
{
    public class StatsController : Controller
    {
        private readonly UsuariosContext _context;

        public StatsController(UsuariosContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> IndexAsync()
            => View(await _context.Stats.ToListAsync());
        
    }
}
