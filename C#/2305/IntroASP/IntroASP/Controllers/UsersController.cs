﻿using IntroASP.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace IntroASP.Controllers
{
    public class UsersController : Controller
    {
        private readonly UsuariosContext _context;

        public UsersController(UsuariosContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> IndexAsync()
            => View(await _context.UsersData.ToListAsync());
        
    }
}
