﻿using System;
using System.Collections.Generic;

namespace IntroASP.Models;

public partial class StatsVM
{
    public string? Nombre { get; set; }

    public string? Dificultad { get; set; }

    public int? Minas { get; set; }

    public double? Tiempo { get; set; }

    public DateTime? Fecha { get; set; }
}
