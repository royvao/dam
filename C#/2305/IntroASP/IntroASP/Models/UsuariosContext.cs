﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

#pragma warning disable CS1030 // Directiva #warning


namespace IntroASP.Models;

public partial class UsuariosContext : DbContext
{
    public UsuariosContext()
    {
    }

    public UsuariosContext(DbContextOptions<UsuariosContext> options)
        : base(options)
    {
    }

    public virtual DbSet<StatsVM> Stats { get; set; }

    public virtual DbSet<UsersDataVM> UsersData { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=USUARIO-PC;Database=Usuarios;TrustServerCertificate=True;Trusted_Connection=True;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<StatsVM>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("estadisticas");

            entity.Property(e => e.Dificultad)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("dificultad");
            entity.Property(e => e.Fecha)
                .HasColumnType("datetime")
                .HasColumnName("fecha");
            entity.Property(e => e.Minas).HasColumnName("minas");
            entity.Property(e => e.Nombre)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("nombre");
            entity.Property(e => e.Tiempo).HasColumnName("tiempo");
        });

        modelBuilder.Entity<UsersDataVM>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__usersDat__3213E83F08A112CD");

            entity.ToTable("usersData");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.UserName)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasColumnName("userName");
            entity.Property(e => e.UserPassword)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasColumnName("userPassword");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
