﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculadora_WPF_v1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public enum OperacionSeleccionada
    {
        Suma,
        Resta,
        Multipliacion,
        Division
    }

    public partial class MainWindow : Window
    {
        double numAnterior, resultado;
       OperacionSeleccionada operacionSeleccionada;

        public MainWindow()
        {
            InitializeComponent();
            limpiarBoton.Click += LimpiarBoton_Click;
            porcentajeBoton.Click += PorcentajeBoton_Click;
            igualBoton.Click += IgualBoton_Click;
        }

        private void IgualBoton_Click(object sender, RoutedEventArgs e)
        {
            double numNuevo;
            if (double.TryParse(resultadoLabel.Content.ToString(), out numNuevo))
            {
                switch (operacionSeleccionada)
                {
                    case OperacionSeleccionada.Suma:
                        resultado = numAnterior + numNuevo;
                        break;
                    case OperacionSeleccionada.Resta:
                        resultado = numAnterior - numNuevo;

                        break;
                    case OperacionSeleccionada.Multipliacion:
                        resultado = numAnterior * numNuevo;

                        break;
                    case OperacionSeleccionada.Division:
                        resultado = numAnterior / numNuevo;

                        break;
                }
            }
            resultadoLabel.Content = $"{resultado}";
        }

        private void OperacionBoton_Click(object sender, RoutedEventArgs e)
        {
            if (double.TryParse(resultadoLabel.Content.ToString(), out numAnterior))
            {
                   resultadoLabel.Content = "0";
            }
            //con esto selecciono la operación que realizaré cuando pulse el boton de igual
            operacionSeleccionada = sender == multiplicacionBoton ? OperacionSeleccionada.Multipliacion : operacionSeleccionada;
            operacionSeleccionada = sender == divisionBoton ? OperacionSeleccionada.Division : operacionSeleccionada;
            operacionSeleccionada = sender == sumaBoton ? OperacionSeleccionada.Suma : operacionSeleccionada;
            operacionSeleccionada = sender == restaBoton ? OperacionSeleccionada.Resta : operacionSeleccionada;
        }

        private void NumeroBoton_Click(object sender, RoutedEventArgs e) 
        {
            int valorSeleccionado = int.Parse((sender as Button).Content.ToString());

            //esto es similar a un if/else
            resultadoLabel.Content = resultadoLabel.Content.ToString() == "0" ? valorSeleccionado.ToString() : $"{resultadoLabel.Content}{valorSeleccionado}";
        }

        private void LimpiarBoton_Click(object sender, RoutedEventArgs e)
        {
            //borrar resultado que se muestra en pantalla
            resultadoLabel.Content = "0";
        }
        private void PorcentajeBoton_Click(object sender, RoutedEventArgs e)
        {
            if (double.TryParse(resultadoLabel.Content.ToString(), out numAnterior))
            {
                numAnterior = numAnterior / 100;
                resultadoLabel.Content = numAnterior.ToString();
            }

        }
    
    }
}
