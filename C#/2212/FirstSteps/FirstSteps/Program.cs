﻿namespace FirstSteps
{
    class Program
    {

        static int mina = 1;
        static int vacio = 0;
        static int usrN, nMinas;
        static void Main(String[] args)
        {
            viewMenu();
            

            Console.Write("Elija la dificultad (1-3): ");
            usrN = int.Parse(Console.ReadLine());

            switch (usrN)
            {
                case 1:
                    usrN = 5;
                    nMinas = 3;
                    break;
                case 2:
                    usrN = 7;
                    nMinas = 5;
                    break;
                case 3:
                    usrN = 10;
                    nMinas = 10;
                    break;
            }

            int turnosSinExplotar = 5;

            bool[,] posicionesMostrar = new bool[usrN, usrN];
            int[,] tablero = new int[usrN, usrN];

            bool minaExplotada = false;
            int turnosSinMinas = 0;

            int n = 0;
            Random r = new Random();
            while (n < nMinas)
            {
                int filaRndm = r.Next(0, tablero.GetLength(0));
                int columnaRndm = r.Next(0, tablero.GetLength(1));

                if (tablero[filaRndm, columnaRndm] == vacio)
                {
                    tablero[filaRndm, columnaRndm] = mina;
                    n++;
                }
            }

            while (!minaExplotada && turnosSinMinas < turnosSinExplotar)
            {
                viewTblr(posicionesMostrar, tablero);

                Console.Write("Fila: ");
                int fila = Convert.ToInt32(Console.ReadLine());
                
                Console.Write("Columna: ");
                int columna = Convert.ToInt32(Console.ReadLine());

                if (tablero[fila, columna] == mina)
                {
                    Console.WriteLine("Explotado");
                    minaExplotada = true;
                    posicionesMostrar[fila, columna] = true;

                }
                else if (posicionesMostrar[fila, columna])
                {
                    Console.WriteLine("Zona ya visitada");
                }
                else
                {
                    Console.WriteLine("Te has librado");
                    turnosSinMinas++;
                    posicionesMostrar[fila, columna] = true;
                }

            }

            if (minaExplotada)
            {
                Console.WriteLine("Has perdido.");
            }
            else
            {
                Console.WriteLine("Has ganado.");

            }
            viewTblr(posicionesMostrar, tablero);



        }

        public static void viewTblr(bool[,] posicionesMostrar, int[,] tablero)
        {
            for (int i = 0; i < posicionesMostrar.GetLength(0); i++)
            {
                for (int j = 0; j < posicionesMostrar.GetLength(1); j++)
                {
                    if (posicionesMostrar[i,j])
                    {
                        if (tablero[i,j] == vacio)
                        {
                            Console.Write(" V ");

                        }
                        else
                        {
                            Console.Write(" M ");
                        }
                    }
                    else
                    {
                        Console.Write(" O ");
                    }
                }
                Console.WriteLine("");
            }
        }

        public static void viewMenu()
        {
            Console.WriteLine("***********************");
            Console.WriteLine("*                     *");
            Console.WriteLine("*      BUSCAMINAS     *");
            Console.WriteLine("*    Creado por RV    *");
            Console.WriteLine("*         2022        *");
            Console.WriteLine("*                     *");
            Console.WriteLine("***********************");

        }
    }
}
