﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;

namespace DesktopApp_v1.ViewModels
{
    class MainViewModel:Conductor<Screen>.Collection.OneActive
    {
		private Visibility _VisibilidadBotonDesplegar = Visibility.Visible;

		public Visibility VisibilidadBotonDesplegar
        {
			get { return _VisibilidadBotonDesplegar; }
			set { _VisibilidadBotonDesplegar = value;
				NotifyOfPropertyChange(() => VisibilidadBotonDesplegar);
			}
		}

		private Visibility _VisibilidadBotonOcultar = Visibility.Hidden;

		public Visibility VisibilidadBotonOcultar
        {
			get { return _VisibilidadBotonOcultar; }
			set { _VisibilidadBotonOcultar = value;
                NotifyOfPropertyChange(() => VisibilidadBotonOcultar);
            }
		}

		public void BotonDesplegarMenu()
		{
			try
			{
				VisibilidadBotonDesplegar = Visibility.Hidden;
				VisibilidadBotonOcultar = Visibility.Visible;
			}
			catch(Exception ex) 
			{

			}
		}

        public void BotonOcultarMenu()
        {
            try
            {
                VisibilidadBotonDesplegar = Visibility.Visible;
                VisibilidadBotonOcultar = Visibility.Hidden;
            }
            catch (Exception ex)
            {

            }
        }
    }
}
