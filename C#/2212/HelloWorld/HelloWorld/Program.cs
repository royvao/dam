﻿namespace HelloWorld
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduzca su nombre: ");
            var nombre = Console.ReadLine();
            var fecha = DateTime.Now;
            Console.WriteLine($"{Environment.NewLine/*Esto es como un salto de línea (\n)*/}¡Hola {nombre}, hoy es {fecha:d}. Hora actual {fecha:t}!");

        }
    }
}