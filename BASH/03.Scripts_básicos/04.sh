#!/bin/bash

//guardamos en una variable la salida del comando last, mostrando sólo el usuario que 
//esté conectado
actual=$(last -p now |awk '{print $1}')

//bucle para que guarde el nombre de los usuarios en user
for user in `ls /home`;do
	
	echo -ne "El usuario $user se ha conectado "; 
	//número de veces que se ha conectado un usuario
	last $user | wc -l ;
done

echo El usuario que está activo ahora es:
echo $actual
