#include <stdio.h>
#include <stdlib.h>

int main () {
	int number;
	float i;
	int *p = NULL;

	/* PREGUNTAR VECTOR */
	printf ("Indique el tamaño del vector: ");
	scanf ("%i", &number);

	p = (int *) malloc (number*sizeof(int));

	/* IMPRIMIR VECTOR */
	printf ("%p\n", p);

	for (i=0; i<number; i++)
		printf ("%.2f\n", i);

	free (p);
	
	return EXIT_SUCCESS;
}

