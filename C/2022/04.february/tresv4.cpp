#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#define N 3

enum TDato { empty, playerOne, playerTwo };
const char *representa[] = {
    "  ",
    "⭕", // 2B55
    "❌", // 274C
};

void draw (unsigned t[N][N]){
    printf ("\n");
    for (unsigned f=0; f<N; f++) {
        printf ("\t");
        for (unsigned c=0; c<N; c++){
            printf (" %s ", representa[t[f][c]]);
            if (c < N-1)
                printf ("│");
        }
        printf ("\n");

        // Imprimir una linea horizontal
        printf ("\t");
        if (f < N-1)
            for (unsigned c=0; c<N; c++) {
                printf ("────");
                if (c < N-1)
                    printf ("┼");
            }
        printf ("\n");
    }

    printf ("\n");
}

void fingir_datos(unsigned t[N][N]) {
    t[1][1] = t[0][0] = playerOne;

    t[1][2] = playerTwo;
}

int main (int argc, char *argv[]) {

    unsigned tablero[N][N];

    /* Inicialización */
    bzero (tablero, sizeof(tablero));
    fingir_datos(tablero);

    draw(tablero);

    return EXIT_SUCCESS;
}

