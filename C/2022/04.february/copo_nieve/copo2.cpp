#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define MAX 300
#define FLAKE "❄" //2744

#define NCOPOS 100
#define NDATOS 4

#define SWIDTH 80
#define SSH     8

const char* sym[] = { "❄", "❅", "❆"};

enum TDato {x, y, vy, car};

void print_copo (unsigned y, int x, int c){

	printf ("\033[%u;%iH%s\n", y , x, sym[c]);
}

int main(){

	int copo[NCOPOS][NDATOS];

	srand (time(NULL));

	/* Inicializar Copos */
	for (int c=0; c<NCOPOS; c++) {
		copo[c][x] = rand () % SWIDTH;
		copo[c][y] = rand () % SSH;
		copo[c][vy] = rand () % 4;
		copo[c][car] = rand () % 3;

	}


	for (unsigned i=0; i<MAX; i++){

	
		printf ("\033[2J");

		for (int a=0; a<NCOPOS; a++){
			copo[a][x] += (rand () % 3) - 1;
			copo[a][y] += copo[a][vy];
			print_copo (copo[a][y] + 1, copo[a][x] + 1, copo[a][car]);   
		}

		usleep(500000);

	}

	return 0;
}


