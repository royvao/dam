#include <stdio.h>
#include <stdlib.h>

#include "algorithm.h"

#define N 8
bool check_horse (enum TPieces black_table[N][N], unsigned row, unsigned col){
	if(
			black_table [row-2][col-1] != empty || black_table [row-2][col+1] != empty ||
			black_table [row-1][col+2] != empty || black_table [row+1][col+2] != empty ||
			black_table [row+2][col-1] != empty || black_table [row+2][col+1] != empty ||
			black_table [row-1][col-2] != empty || black_table [row+1][col-2] != empty
	  ){
		return true;
	}else{
		return false;
	}
}
