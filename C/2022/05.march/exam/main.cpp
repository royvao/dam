#include <stdio.h>
#include <stdlib.h>

#include "interface.h"
#include "algorithm.h"

#define N 8

void init (enum TPieces black_table[N][N]){
	for (int i=0; i<8; i++){
		black_table[0][i] = tower;
	}
	for (int f=0; f<8; f++){
		black_table[1][f] = pawn;
	}
}

int main (){

	unsigned black_table[N][N];
	unsigned *row;
	unsigned *col;
	
//	black_table = (int*) malloc (N*N*sizeof(int));
	init (black_table);
	ask_position (row, col);
	check_horse (black_table, row, col);
//	free (black_table);

	return EXIT_SUCCESS;
}




