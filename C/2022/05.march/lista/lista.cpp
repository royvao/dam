#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

#define		MAXPAL 		100
#define 	PAL 		5

int main (){
	
	/* DECLARAR VARIABLES */
	unsigned len;
	char buff[MAXPAL];
	char *lista[PAL];
	
	/* INICIALIZACIÓN */ 
	

	/* ENTRADA DE DATOS */
	for (unsigned i=0; i<PAL; i++){
		system ("clear");
		printf ("Palabra %i: ", i+1);
		scanf (" %[^\n]", buff);
		__fpurge (stdin);
		len = strlen(buff) + 1;
		lista[i] = (char *) malloc (len);
		strcpy (lista[i], buff);
	}
	
	/* CALCULOS */
	

	/* SALIDA DE DATOS*/
	printf ("\n");
	for (unsigned i=0; i<PAL; i++)
		printf ("%i.- %s\n", i+1, lista[i]);
	printf ("\n");

	/* LIBERACION */
	for (unsigned i=0; i<PAL; i++)
		free (lista[i]);

	return EXIT_SUCCESS;
}
