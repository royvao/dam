#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define MAX 30
int main(){

	int x;
	const char* c = "*";

	srand (time(NULL));

	for (unsigned i=0; i<MAX; i++){

		x = (rand () % 3)-1;
		printf ("\033[2J");
		printf ("\033[%u;%iH%s\n", i , 40+x , c);
		usleep(500000);

	}

	return 0;
}


