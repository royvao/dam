#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>
#include <cstring>

int main () {
	int a[5][10];
	int row;
	int column;

	for (row=0; row<5; row++){
		for (column=0; column<10; column++){
			if (row == 0 && column !=9){
				a[row][column] = column;
				printf ("%i ", a[row][column]);
			}
			if (row !=0 && column !=9){
				a[row][column] = a[row-1][column+1] + a[row-1][column];
				printf ("%i ", a[row][column]);
			}
			if (column == 9){
				a[row][column] = column;
				printf ("%i \n", a[row][column]);
			}
		}
	}
	return EXIT_SUCCESS;
}

