#include <stdio.h>
#include <stdlib.h>

int main () {
	int number;
	int row;
	int column;

	printf ("Introduzca el largo del lado para un cuadrado: ");
	scanf ("%d", &number);

	for (row = 1; row <= number; row++){
		for (int column = 1; column <= number; column++){
		       if (row == 1 || row == number || column == 1 || column == number){
		       printf ("*");
		       }
			else{
		 		printf (" ");
			}
		}
	printf ("\n");
	}	
	return EXIT_SUCCESS;
}

