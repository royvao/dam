#include <stdio.h>
#include <stdlib.h>

int main () {
	float positiveNumber;
	int addedNumber = 1;
	float average;	
	float numberSum = 0;

	do{
		printf ("Introduce un número positivo: ");
		scanf ("%f", &positiveNumber);
		numberSum = numberSum + positiveNumber;
		addedNumber++;
	}
	while (positiveNumber > 0);
		average = numberSum / addedNumber;
		printf ("La media es %.2f\n", average);

	return EXIT_SUCCESS;
}

