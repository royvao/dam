#include <stdio.h>
#include <stdlib.h>

int main () {
	char userChar;
	int number;

	printf ("Introduce un carácter: ");
	scanf ("%c", &userChar);
	printf ("¿Cuántas veces quiere que se repita?: ");
	scanf ("%i", &number);

	for (int i=0; i<number; i++){
		printf ("%c ", userChar);
	}
	printf ("\n");
	return EXIT_SUCCESS;
}

