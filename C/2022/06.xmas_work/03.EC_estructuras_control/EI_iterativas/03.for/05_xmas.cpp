#include <stdio.h>
#include <stdlib.h>

int main () {
	int numberQuantity = 10;
	int numbers;
	int increment = 0;
	int sum = 0;
	double average = 0.00;
	double numberAverage;

	printf ("Vamos a calcular la media de 10 números\n");

	for (int i = 0 ; i < numberQuantity; i++){
		printf ("Indique su %iº número: ", ++increment);
		scanf ("%i", &numbers);

		sum = sum + numbers;
	}
	numberAverage = numberQuantity;
	average = sum / numberAverage;
	
	printf ("La media es %.2lf\n", average);

	return EXIT_SUCCESS;
}

