#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

#define MAX 10
int main () {
	char userAnswer;
	char cat;
	char human;

	printf ("¿Es un humano o es un gato? h/g: ");
	scanf ("%c", &userAnswer);

	if (userAnswer == 'h'){
		printf ("¿Tiene tos? y/n: ");
		__fpurge(stdin);
		scanf("%c", &human);
	}
	if (human == 'y'){
		printf ("Es un humano con tos ferina\n");
	}
	if (human == 'n'){
		printf ("Es un humano sano\n");
	}
	if (userAnswer == 'g'){
		printf ("¿Tiene tos? y/n: ");
		__fpurge(stdin);
		scanf("%c",&cat);
	}
	if (cat == 'y'){
		printf ("Es un gato con tos felina\n");
	}
	if (cat == 'n'){
		printf ("Es un gato que maúlla\n");
	}
	return EXIT_SUCCESS;
}

