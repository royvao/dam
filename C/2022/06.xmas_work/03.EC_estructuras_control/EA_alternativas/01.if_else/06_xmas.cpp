#include <stdio.h>
#include <stdlib.h>
#include <cstring>

int main () {
	const float E = 0.001;
	float number;

	printf ("Escriba un número real: ");
	scanf ("%f", &number);

	if (number > 3-E && number < 3+E){
		printf ("El número está en el entorno\n");
	}
	else{
		printf ("El número no está en el entorno\n");
	}
	return EXIT_SUCCESS;
}

