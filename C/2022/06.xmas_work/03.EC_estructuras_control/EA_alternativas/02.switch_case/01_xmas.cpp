#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>
#include <cstring>
#include <string.h>

#define MAX 30
int main () {
	int number;

	printf ("1.- Directorio a su elección\n");
	printf ("2.- Ocupación de un directorio y un subdirectorio\n");
	printf ("3.- Espacio de cada disco\n");
	printf ("4.- Ocupación de memoria\n");
	printf ("Introduce el número de la opción que quiera consultar: ");
	scanf ("%i", &number);

	char directory[MAX];
	char command[MAX] = "ls -lh";

	switch (number){
		case 1:
			__fpurge(stdin);
			printf ("Introduce el directorio que quieras ver: ");
			fgets (directory, MAX, stdin);

			strcat (command, " ");
			strcat (command, directory);

			system (command);

		break;
		case 2:
			system ("du -sh");
		break;
		case 3:
			system ("du -h");
		break;
		case 4:
			system ("free");
		break;
	}

	return EXIT_SUCCESS;
}

