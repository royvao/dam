#include <stdio.h>
#include <stdlib.h>

int main () {
	int a = 0;
	char b = 6;

	while (b > 0){
		b >>= 1;
		b |= (a = a++ %2);
		printf ("A=%i"" B=%i\n", a, b);
	}

	return EXIT_SUCCESS;
}

