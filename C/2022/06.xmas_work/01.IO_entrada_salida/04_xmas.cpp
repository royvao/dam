#include <stdio.h>
#include <stdlib.h>

#define MAX 30
int main () {
	char number[MAX];
	
	printf ("Escribe un número hexadecimal: ");
	scanf (" %4[0-9a-fA-F]", &number);
	printf ("El número introducido es: %s\n", number);

	return EXIT_SUCCESS;
}

