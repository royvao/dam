#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>
#include <unistd.h>
#include <string.h>

#define MAX 30
int main () {
	char line[MAX];
	__fpurge (stdin);
	
	printf ("Esto es una frase de prueba: \n");
	fgets(line, MAX, stdin);
	
	char scanLine = strlen (line);
	line [scanLine-1] = '\0';

	printf ("La frase de prueba es: \n");
	puts(line);

	return EXIT_SUCCESS;
}

