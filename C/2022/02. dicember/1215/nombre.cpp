#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>

#define MAX 0x20

int main (int argc, char *argv[]){
	
	char nombre[MAX];

	printf ("Nombre: ");
	scanf (" %s", nombre);
	printf ("Su nombre es %s\n", nombre);
	
	__fpurge (stdin);
	
	printf("Nombre: ");
	fgets (nombre, MAX, stdin);
	
	int ultima = strlen (nombre);
	nombre[ultima-1] = '\0';

	printf ("Su nombre es %s\n", nombre);

	printf ("Nombre (yo=<nombre>): ");
	scanf (" yo = %s", nombre);
	printf (" %s\n", nombre);

	printf ("Dos palabras: ");
	scanf (" %*s %s", nombre);
	printf ("Segunda: %s\n", nombre);

	char myName[MAX];
	char hexa[MAX];

	printf ("Hexa Number: ");
	scanf (" %8[0-9a-fA-F]", hexa);
	__fpurge (stdin);
	printf ("hexa: %s\n", hexa);

	printf ("Nombre: ");
	scanf (" %[^\n]", myName);
	printf ("Nombre: %s\n",myName);
	__fpurge (stdin);

	char * mn;
	printf ("Nombre: ");
	scanf (" %m[^\n]", &mn);
	printf ("Nombre "
		"\x1b[33m%s \x1b[0m"
		".\n", mn);
	free (mn); //libera memoria
	

	return EXIT_SUCCESS;
}
