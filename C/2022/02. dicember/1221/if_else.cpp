#include <stdio.h>
#include <stdlib.h>

//Predicado
int esMultiploDe (int valor, int multi){
	return valor % multi == 0;
}

int main (int argc, char *argv[]) {
	int numero;

	printf ("Número: ");
	scanf ("%i", &numero);

	if (numero == 3) // Si numero es igual a 3
		printf ("Has puesto 3.\n");

	if (numero % 2 == 0) // Si numero tiene resto 0 al dividirlo entre 2
		printf ("El número %i es par.\n", numero);
	else
		printf ("El número %i es impar.\n", numero);
	
	if (numero) // Poner "numero" es lo mismo que --> numero!=0 
		printf ("El número %i es distinto de 0.\n", numero);
	
	if (!numero) // Convertir verdadero a falso y viceversa (operador lógico NOT)
		printf ("%i es igual de 0.\n", numero);

	if (esMultiploDe(numero, 3))
		printf ("%i es múltiplo de 3.\n", numero);

	//Operadores

	return EXIT_SUCCESS;
}
