#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <wait.h>

int main (int argc, char *argv[]){
	pid_t pid;
	int tuberia[2];

	pipe(tuberia);
	if(argc==3){
		if(pid==-1){
			printf("error\n");
		}else{
			if(pid==0){
				//hijo
				close(1);
				dup(tuberia[0]);
				//dup2(tuberia[1], STDOUT_FILENO);
				close(tuberia[0]);
				close(tuberia[1]);
				execlp(argv[2], argv[2],NULL);

			}else{
				//padre
				close(0);
				dup(tuberia[0]);
				//dup2(tuberia[1], STDOUT_FILENO);
				close(tuberia[0]);
				close(tuberia[1]);
				execlp(argv[1], argv[1],NULL);
				wait(NULL);
			}
		}
	}else{
		printf("error parametros\n");
	}
	return 0;
}
