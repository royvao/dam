#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[]){
	
	FILE *fichero;
	char cadena[200];

	if (mkfifo("tuberia7", 0666) != 0){
		printf("Error, tubería existente\n");
	}

	fichero=fopen("tuberia7", "r");
	
	while(!feof(fichero)){
		fgets(cadena, 200, fichero);
		printf ("Los datos recibidos %s\n", cadena);		
	}

	fclose(fichero);
	

	return 0;
}
