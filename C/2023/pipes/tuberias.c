#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>


int main(int argc, char *argv[]){
	pid_t pid;
	int tuberia[2], bytesleidos;
	char cadena[50], buffer[SIZE];

	pid=fork();

	if(pid==-1){
		printf("Error\n");
	}else{
		if(pid==0){
			//hijo
			close(tuberia[1]);
			while((bytesleidos=read(tuberia[0], buffer, SIZE))>0){
				printf("Lo que leo de la tuberia%s\n", buffer);
			}
			close(tuberia[0]);
		}else{
			//padre
			close(tuberia[0]);
			strcpy(cadena, "Escribe a mi hijo\n");
			write(tuberia[1], cadena, strlen(cadena));
			close(tuberia[1]);
			wait(NULL);
		}
	}

	return 0;
}
