#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>

void manejadora(int signal){
	printf("Fallecido x_x\n");
	exit(0);
}

int main (int argc, char *argv[]){
	pid_t pid;
	pid = fork();
	int status;

	struct sigaction new;
	struct sigaction old;

	new.sa_handler=&manejadora;

	if(pid==-1){
		printf("Error\n");
	}else{
		if(pid==0){
			sigaction(SIGUSR1, &new, &old);
			while(1){
				printf("Esperando la papaseñal\n");
				pause();
			}
		}else{
			sleep(10);
			kill(pid, SIGUSR1);
			wait(&status);
		}
	}

	return 0;
}

