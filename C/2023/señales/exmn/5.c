#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>

int main(int argc, char *argv[]){
	int pid, sig;

	if (argc==3){
		sig=atoi(argv[1]);
		pid=atoi(argv[2]);

		if(kill(0,pid)==1){
			printf("El proceso no existe\n");
		}else{
			printf("El proceso se ha muerto\n");
			kill(sig,pid);
		}
	}else{
		printf("Los parámetros introducidos son incorrectos\n");
	}
	return 0;
}
