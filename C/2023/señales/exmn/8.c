#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>

void manejadora(int signal){
	printf ("Hijo muerto.\n");
	exit(0);
}

int main(int argc, char *argv[]){
	pid_t pid;
	pid=fork();
	int status;

	if (pid==-1){
		printf("Error\n");
	}else{
		if(pid==0){
			//hijo
			signal(SIGINT, &manejadora);
			printf("Hijo creado.\n");
			pause();
		}else{
			//padre
			sleep(5);
			kill(pid, SIGINT);
			wait(&status);
		}

	}
	return 0;
}
