#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>

int main (int argc, char *argv[]){

	pid_t pid, pid_hijo;
	int estado;

	pid=fork();

	if(pid==-1){
		printf ("Ha ocurrido un error durante la creación del hijo\n");
	}else{
		if(pid==0){
			//Proceso hijo
			printf("El pid del hijo es %i y del padre es %i\n", getpid(), getppid());
			exit(10);
		}else{
			//Proceso padre
			printf("El pid del padre es %i y del hijo es %i\n", getpid(), pid);
			//NULL no recoge datos del hijo y wait devuelve pid del hijo
			pid_hijo=wait(&estado);
			//El valor del dato que envía el hijo del bit 8 al 15
			printf("El pid del hijo que devuelve wait es %i y el valor del estado es %i\n", pid_hijo, estado>>8);
		}
	}
	
}

