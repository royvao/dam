#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>

int main (){

	char abcd[] ="abcdefghijklmnopqrstuvwxyz";
	int letra=0;
	pid_t pid=1;

	for(int i=0;i<=5;i++){
		//printf("Parte que solo va a realizar el padre %i y vuelta del bucle %i\n", getpid(), i);
		//printf ("Se divide aqui el programa\n");
		//if(pid==0){
		printf ("La letra que le corresponde al hijo es: %c.\n"
			"El pid del proceso es: %i.\n"
			"La posicion %i.\n", abcd[letra], getpid(),letra);
		printf("\n\n");	
		letra++;
		//exit(0);
		//}else{
		//printf("Es el padre %i y valor de letra %i\n", getpid(), letra);
		//}
	}
}
