# DAM PERSONAL PROJECT
With primary projects of my programation study
## LANGUAGES
- ANDROID
    - JAVA
    ```JAVA
        System.out.println("Hello, World!");
    ```
    - Kotlin
    ```JAVA
        println("Hello, World!")
    ```   
- BASH
    - Shell Script
    ```bash
        #!/bin/bash
        echo Hello, World!
    ```
- Others
    - C
    ```C
        printf("Hello, World!");
    ```
    - C++
    ```C++
        std::cout << "Hello, World!";
    ```
    - C#
    ```C#
        Console.WriteLine("Hello, World!");
    ```
