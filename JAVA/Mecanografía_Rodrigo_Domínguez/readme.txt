A TENER EN CUENTA:
	- En la pantalla de juego, el teclado acepta teclas como el Ctrl, Alt, teclas de función...y se consideran errores.
	- No se pueden introducir mayúsculas ni tildes en la lección, tampoco símbolos.
	- No se almacenan las estadísticas en el fichero "stats.txt", por lo que tampoco se lee nada de este.
	- En los ficheros de usuario y estadísticas, el separador es ";" entre variables y "_" entre usuarios diferentes.
	- En el fichero de textos, el separados utilizado es el cambio de línea.
	- Al terminar la lección, se debe pulsar una tecla extra para finalizar. Esta no se contará como fallo.