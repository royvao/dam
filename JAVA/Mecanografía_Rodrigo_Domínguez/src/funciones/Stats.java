package funciones;

import java.util.Date;

public class Stats {
	private String name;
	private Date fecha;
	private int time, pulsations, fails;
	
	public Stats(String name, Date fecha, int time, int pulsations, int fails) {
		
		this.name = name;
		this.fecha = fecha;
		this.time = time;
		this.pulsations = pulsations;
		this.fails = fails;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public int getPulsations() {
		return pulsations;
	}

	public void setPulsations(int pulsations) {
		this.pulsations = pulsations;
	}

	public int getFails() {
		return fails;
	}

	public void setFails(int fails) {
		this.fails = fails;
	}

	@Override
	public String toString() {
		return "Stats [name=" + name + ", fecha=" + fecha + ", time=" + time + ", pulsations=" + pulsations + ", fails="
				+ fails + "]";
	}
	
	public String toStringName() {
		return name;
	}
	public Date toStringFecha() {
		return fecha;
	}
	public int toStringTime() {
		return time;
	}
	public int toStringPulsations() {
		return pulsations;
	}
	public int toStringFails() {
		return fails;
	}
}
