package funciones;

public class Users {
 private String name, pwd, mail;

public Users(String name, String pwd, String mail) {
	
	this.name = name;
	this.pwd = pwd;
	this.mail = mail;
}

public String getNombre() {
	return name;
}

public void setNombre(String name) {
	this.name = name;
}

public String getPwd() {
	return pwd;
}

public void setPwd(String pwd) {
	this.pwd = pwd;
}

public String getMail() {
	return mail;
}

public void setMail(String mail) {
	this.mail = mail;
}

@Override
public String toString() {
	return "Users [name=" + name + ", pwd=" + pwd + ", mail=" + mail + "]";
}

public String toStringName() {
	return name;
}
public String toStringMail() {
	return mail;
}
public String toStringPwd() {
	return pwd;
}
}
