package traductor2;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class Clase_Panel extends Principal{

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Clase_Panel window = new Clase_Panel();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Clase_Panel() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmRoyTranslate = new JFrame();
		frmRoyTranslate.setTitle("Roy translate 2.0");
		frmRoyTranslate.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\CFGS\\work\\DAM\\java\\traductor\\traductor2\\src\\icono.jpg"));
		frmRoyTranslate.setBounds(100, 100, 450, 300);
		frmRoyTranslate.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmRoyTranslate.getContentPane().setLayout(null);
		
		JLabel Titulo = new JLabel("TRADUCTOR 2.0");
		Titulo.setHorizontalAlignment(SwingConstants.CENTER);
		Titulo.setFont(new Font("Tahoma", Font.BOLD, 18));
		Titulo.setBounds(0, 11, 434, 43);
		frmRoyTranslate.getContentPane().add(Titulo);
	}

}
