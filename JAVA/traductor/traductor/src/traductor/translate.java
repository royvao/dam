package traductor;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Toolkit;
/*import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;*/
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import java.awt.Color;

public class translate {

	private JFrame frmTraductor;
	private JTextField textField_traducir;
	private JTextField textField_traducido;
	/*private JComboBox combo;*/
	private JLabel lblElegirIdioma;
	private JLabel lblNewLabel_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					translate window = new translate();
					window.frmTraductor.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public translate() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTraductor = new JFrame();
		frmTraductor.getContentPane().setBackground(new Color(196, 232, 234));
		frmTraductor.setResizable(false);
		frmTraductor.setTitle("Roy translate");
		frmTraductor.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\CFGS\\work\\DAM\\java\\traductor\\traductor\\src\\icono.jpg"));
		frmTraductor.setBounds(100, 100, 450, 300);
		frmTraductor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTraductor.getContentPane().setLayout(null);
		
		lblNewLabel_1 = new JLabel("TRADUCTOR");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_1.setBounds(0, 11, 434, 43);
		frmTraductor.getContentPane().add(lblNewLabel_1);
		
		textField_traducir = new JTextField();
		textField_traducir.setBounds(120, 76, 201, 36);
		frmTraductor.getContentPane().add(textField_traducir);
		textField_traducir.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Español");
		lblNewLabel.setBounds(28, 78, 72, 33);
		frmTraductor.getContentPane().add(lblNewLabel);
		
		JButton boton_traducir = new JButton("✔");
		boton_traducir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField_traducir.getText();
				traducir(textField_traducir.getText());
				
			}

			private void traducir(String text) {
				Document web = null;
				String url = "https://www.spanishdict.com/translate/"+text;
				try {
					web = Jsoup.connect(url).get();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				Elements palabras = web.getElementById("quickdef1-es").getElementsByClass("a--tds4TDh9");
				System.out.println(palabras.html());
				textField_traducido.setText(palabras.html().toUpperCase());				
			}
		});
		boton_traducir.setBounds(331, 77, 66, 35);
		frmTraductor.getContentPane().add(boton_traducir);
		
		textField_traducido = new JTextField();
		textField_traducido.setBounds(120, 157, 276, 38);
		frmTraductor.getContentPane().add(textField_traducido);
		textField_traducido.setColumns(10);
		
		/*combo = new JComboBox();
		combo.setModel(new DefaultComboBoxModel(new String[] {"Inglés", "Francés", "Alemán"}));
		combo.setToolTipText("");
		combo.setBounds(28, 157, 122, 38);
		frmTraductor.getContentPane().add(combo);*/
		
		lblElegirIdioma = new JLabel("Inglés");
		lblElegirIdioma.setBounds(28, 160, 72, 33);
		frmTraductor.getContentPane().add(lblElegirIdioma);
		

	}
}
