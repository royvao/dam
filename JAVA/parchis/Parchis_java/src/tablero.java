import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.border.EtchedBorder;

public class tablero {

	private JFrame frmParchisStar;
	static private JPanel panel_rojo, panel_amarillo, panel_verde, panel_azul;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					tablero window = new tablero();
					window.frmParchisStar.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public tablero() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmParchisStar = new JFrame();
		frmParchisStar.setTitle("PARCHIS STAR");
		frmParchisStar.setBounds(100, 100, 450, 300);
		frmParchisStar.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmParchisStar.getContentPane().setLayout(new GridLayout(2, 2, 0, 0));

		Image icono = null;
		try {
			icono = ImageIO.read(new File("src/icono.ico"));

		} catch (IOException e1) {
			e1.printStackTrace();
		}
		frmParchisStar.setIconImage(icono);

		panel_rojo = new JPanel();
		panel_rojo.setBackground(new Color(255, 0, 0));
		frmParchisStar.getContentPane().add(panel_rojo);

		JButton boton_rojo = new JButton("   Ver amarillo  ");
		boton_rojo.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));

		panel_rojo.add(boton_rojo);
		panel_amarillo = new JPanel();
		panel_amarillo.setVisible(false);
		panel_amarillo.setBackground(new Color(255, 255, 0));
		frmParchisStar.getContentPane().add(panel_amarillo);

		JButton boton_amarillo = new JButton("   Ver verde   ");
		boton_amarillo.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));

		panel_amarillo.add(boton_amarillo);

		panel_verde = new JPanel();
		panel_verde.setVisible(false);
		panel_verde.setBackground(new Color(128, 255, 0));
		frmParchisStar.getContentPane().add(panel_verde);

		JButton boton_verde = new JButton("   Ver azul   ");
		boton_verde.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));

		panel_verde.add(boton_verde);

		panel_azul = new JPanel();
		panel_azul.setVisible(false);
		panel_azul.setBackground(new Color(0, 255, 255));
		frmParchisStar.getContentPane().add(panel_azul);

		JButton boton_azul = new JButton("   Ver rojo   ");
		boton_azul.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));

		panel_azul.add(boton_azul);

		boton_rojo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_amarillo.setVisible(true);
			}
		});
		boton_amarillo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_verde.setVisible(true);
			}
		});
		boton_verde.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_azul.setVisible(true);
			}
		});
		boton_azul.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_amarillo.setVisible(false);
				panel_verde.setVisible(false);
				panel_azul.setVisible(false);
			}
		});

	}
}
