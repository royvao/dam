package UML_vehiculos;

import java.util.ArrayList;
import java.util.Scanner;

public class Menu {
	
	static Scanner leer = new Scanner(System.in);
	public static ArrayList<Vehiculo> vehiculos;
	static int userOption, parar;
	
	public static void menu(ArrayList<Vehiculo> vehiculos, ArrayList<Avion> aviones,  ArrayList<Coche> coches) {
	
		do {
				PrintMenu.main();
		try {
					userOption = leer.nextInt();
					leer.nextLine();
					if(userOption <=6 && userOption >0) {
					switch(userOption) {
					case 1:
						CrearVehiculo.crearCoche();
						parar = CrearVehiculo.crearMas();
						break;
					case 2:
						CrearVehiculo.crearAvion();
						parar = CrearVehiculo.crearMas();
						break;
					case 3:
						CrearVehiculo.crearBarco();
						parar = CrearVehiculo.crearMas();
						break;
					case 4:
						Info.velocidadMax(vehiculos, aviones, coches);
						parar = CrearVehiculo.crearMas();
						break;
					case 5:
						Info.verElectricos(vehiculos, aviones, coches);
						parar = CrearVehiculo.crearMas();
						break;
					case 6: 
						System.out.println("Gracias por utilizar nuestro servicio.");
						parar = 1;
						break;
					}
					}else{
					System.out.println("El caracter introducido no es un número del 1 al 6\n");
					Menu.menu(vehiculos,aviones, coches);
					}
				} catch (Exception e) {
					leer.nextLine();
					System.out.println("El caracter introducido no es un número entre 1 al 6\n");
					Menu.menu(vehiculos, aviones, coches);

							}
						
					}
					while(parar==0);
					do {
						System.out.println("");
						CrearVehiculo.verVehiculos();
						
						System.out.println("¡Gracias!");
						System.exit(0);
			
					}
					while(parar==1);
	}
}

		
	



