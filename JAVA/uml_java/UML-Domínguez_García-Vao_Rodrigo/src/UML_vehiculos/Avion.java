package UML_vehiculos;

public class Avion extends Vehiculo implements Comparable<Avion>{

		private byte motores;
		private double velocidadMax;
		private boolean combate;
	
		public Avion(byte motores, double velocidadMax, boolean combate,String marca, String color, String numBastidor, int kilometros, int añoFabricacion) {
		super(marca, color, numBastidor, kilometros, añoFabricacion);
		this.motores=motores;
		this.velocidadMax=velocidadMax;
		this.combate=combate;
		
	}

		private Avion(String marca, String color, String numBastidor, int kilometros, int añoFabricacion,
				boolean combate) {
			super(marca, color, numBastidor, kilometros, añoFabricacion);
			this.combate = combate;
		}

		public byte getMotores() {
			return motores;
		}

		public void setMotores(byte motores) {
			this.motores = motores;
		}

		public double getVelocidadMax() {
			return velocidadMax;
		}

		public void setVelocidadMax(double velocidadMax) {
			this.velocidadMax = velocidadMax;
		}

		public boolean isCombate() {
			return combate;
		}

		public void setCombate(boolean combate) {
			this.combate = combate;
		}

		@Override
		public String toString() {
			return "Avion [motores=" + motores + ", velocidadMax=" + velocidadMax + ", combate=" + combate + ", marca="
					+ marca + ", color=" + color + ", numBastidor=" + numBastidor + ", kilometros=" + kilometros
					+ ", añoFabricacion=" + añoFabricacion + "]";
		}

		@Override
		public int compareTo(Avion o) {
			if(this.velocidadMax>o.velocidadMax) {
				return -1;
			}
			if(this.velocidadMax<o.velocidadMax) {
				return 1;
			}

			return 0;
		}
		
	

}
