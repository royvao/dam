package UML_vehiculos;

import java.util.ArrayList;
import java.util.Collections;

public class Info {
	
	public static void verElectricos(ArrayList<Vehiculo> vehiculos, ArrayList<Avion> aviones,  ArrayList<Coche> coches) {
		
	boolean noElectrico = true;
	
		if(coches==null || coches.size()==0) {
			System.out.println("\n No existe ningún coche, debe crear uno.");
		}else {
			System.out.println("Listado de los coches eléctricos: \n");
			for(int i=0; i<coches.size(); i++) {
				if(((Coche) coches.get(i)).isElectrico() == true) {
					System.out.println(coches.get(i).toString());
					noElectrico = false;
				}
				
			}
			if(noElectrico == true) {
				System.out.println("No hay vehículos eléctricos.");
				Menu.menu(vehiculos, aviones, coches);
			}
		}
	}
	
	public static void velocidadMax(ArrayList<Vehiculo> vehiculos, ArrayList<Avion> aviones,  ArrayList<Coche> coches) {
		if(aviones==null || aviones.size()==0) {
			System.out.println("\nNo existe ningún avión creado, por favor cree uno antes.");
			Menu.menu(vehiculos, aviones, coches);
		}else {
			Collections.sort(aviones);
			System.out.println("El avión más rápido es: \n" + aviones.get(0).toString());
		}
		
	}


}
