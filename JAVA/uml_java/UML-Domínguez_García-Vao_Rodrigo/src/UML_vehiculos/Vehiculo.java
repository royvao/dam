package UML_vehiculos;

public class Vehiculo {
	public String marca, color, numBastidor;
	public int kilometros;
	protected int añoFabricacion;
	
	
	public Vehiculo(String marca, String color, String numBastidor, int kilometros, int añoFabricacion) {
		super();
		this.marca = marca;
		this.color = color;
		this.numBastidor = numBastidor;
		this.kilometros = kilometros;
		this.añoFabricacion = añoFabricacion;
	}


	public String getMarca() {
		return marca;
	}


	public void setMarca(String marca) {
		this.marca = marca;
	}


	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}


	public String getNumBastidor() {
		return numBastidor;
	}


	public void setNumBastidor(String numBastidor) {
		this.numBastidor = numBastidor;
	}


	public int getKilometros() {
		return kilometros;
	}


	public void setKilometros(int kilometros) {
		this.kilometros = kilometros;
	}


	public int getAñoFabricacion() {
		return añoFabricacion;
	}


	public void setAñoFabricacion(int añoFabricacion) {
		this.añoFabricacion = añoFabricacion;
	}


	@Override
	public String toString() {
		return "Vehiculo [marca=" + marca + ", color=" + color + ", numBastidor=" + numBastidor + ", kilometros="
				+ kilometros + ", añoFabricacion=" + añoFabricacion + "]";
	}
	
	
	
}
