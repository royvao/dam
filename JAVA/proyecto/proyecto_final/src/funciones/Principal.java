package funciones;

import java.awt.EventQueue;

import javax.swing.JOptionPane;

import entorno.*;


public class Principal {
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// LLAMAMOS AL FRAME
					new MFrame();
					
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "¡UPS! Ha ocurrido un error inesperado. Se debe reiniciar la aplicación.", "Error.", JOptionPane.CLOSED_OPTION);
				}
			}
		});

	}

}
