package funciones;

import java.io.*;

import java.util.Scanner;


import javax.swing.JOptionPane;

public class Fnc_users {

	// FUNCIÓN PARA LEER EL FICHERO DE USUARIOS Y SEPARAR LAS PALABRAS CON UN DELIMITADOR
	public static Users[] readUsers(File nombre) {
		Users[] usuarios = null;
		
		try {
			FileReader fr = new FileReader(nombre);
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(fr);
			String linea;
			
			linea = br.readLine();
			
			// SEPARADOR ;
			String[] tabla = linea.split("_");
			usuarios = new Users[tabla.length];

			for(int i=0; i < tabla.length; i++) {
				String[] datos = tabla[i].split(";");
				usuarios[i] = new Users(datos[0], datos[1], datos[2]);
			}
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Se ha detectado un error.", "Error.",JOptionPane.CLOSED_OPTION);
			return null;
		}
		
		return usuarios;

	}
	
	// FUNCIÓN PARA COMPROBAR QUE EL FICHERO EXISTE
	public static File checkFile(String filesRute) {
		File fileUsr = null;
		
		fileUsr = new File(filesRute);
		if(!fileUsr.exists()) {
			JOptionPane.showMessageDialog(null, "Falta un fichero.", "Error.",JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
		return fileUsr;
	}
	
	// FUNCIÓN PARA COMPROBAR EL NOMBRE DE USUARIO Y LA CONTRASEÑA

	
	//FUNCION PARA LEER EL FICHERO DE TEXTOS
	public static String[] readFiles(File textFile) {
		String[] textos = new String[2];
		
		try {
			@SuppressWarnings("resource")
			Scanner scan = new Scanner(textFile);
			
			while(scan.hasNextLine()) {
				if(textos[0] == null) {
					textos[0]=scan.nextLine();
				}else if(textos[1] == null) {
					textos[1]=scan.nextLine();					
				}
			}
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Se ha detectado un error.", "Error.",JOptionPane.CLOSED_OPTION);
		}
		return textos;
	}
	

	

}
