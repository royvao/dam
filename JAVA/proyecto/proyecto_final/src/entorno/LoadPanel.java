package entorno;

import java.awt.Color;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;

public class LoadPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private JProgressBar pgb;
	private JLabel titulo, fondo;
	String filesRute;

	public LoadPanel(MFrame frame) {
		// PROPIEDADES DEL PANEL
		setVisible(true);
		setLayout(null);
		setBackground(new Color(255, 255, 255));

		// PROPIEDADES DEL LABEL "TITULO"
		titulo = new JLabel("Curso de mecanografía");
		titulo.setHorizontalAlignment(SwingConstants.CENTER);
		titulo.setFont(new Font("Times New Roman", Font.BOLD, 30));
		titulo.setBounds(20, 10, 410, 50);
		add(titulo);

		// PROPIEDADES DE LA BARRA DE PROGRESO
		pgb = new JProgressBar();
		pgb.setSize(410, 35);
		pgb.setLocation(titulo.getLocation().x, titulo.getLocation().y + 250);
		pgb.setForeground(new Color(128, 128, 255));
		pgb.setBackground(new Color(128, 0, 255));
		pgb.setStringPainted(true);
		add(pgb);

		// PROPIEDADES DEL LABEL "FONDO"
		fondo = new JLabel("");
		fondo.setIcon(new ImageIcon("images\\rv-logo.jpg"));
		fondo.setBounds(62, 54, 330, 179);
		add(fondo);


	}

	public JProgressBar getPgb() {
		return pgb;
	}

	public void setPgb(JProgressBar pgb) {
		this.pgb = pgb;
	}

}