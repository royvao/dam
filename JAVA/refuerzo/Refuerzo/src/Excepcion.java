import java.io.File;
import java.io.IOException;

public class Excepcion {

	public static void main(String[] args) {
		
		try {
			
			File 
			codigo 		= new File ("codigo"),
			texto 		= new File (codigo,"texto"),
			doc 		= new File (texto,"doc"),
			newfile 	= new File (doc,"test.txt");
			
			codigo.mkdir();
			texto.mkdir();
			doc.mkdir();
			newfile.createNewFile();
	
		} catch (IOException e){
			
			System.out.println(e.getMessage());		
			
		}
	}
}
